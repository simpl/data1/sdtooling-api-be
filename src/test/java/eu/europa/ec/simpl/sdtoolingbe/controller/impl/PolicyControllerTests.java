package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider.AuthenticationProviderClient;
import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.model.client.accesspolicy.AccessPolicyRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.client.identityattribute.IdentityAttribute;
import eu.europa.ec.simpl.sdtoolingbe.model.client.policyaction.PolicyAction;
import eu.europa.ec.simpl.sdtoolingbe.model.client.usagepolicy.UsagePolicyRequest;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;
import eu.europa.ec.simpl.sdtoolingbe.service.policy.PolicyService;
import eu.europa.ec.simpl.sdtoolingbe.service.usersroles.UsersRolesService;
import eu.europa.ec.simpl.sdtoolingbe.service.usersroles.UsersRolesServiceImpl;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationServiceImpl;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;

class PolicyControllerTests {
	
	private static final String VALID_JWT = TestSupport.createValidJwt();
			
	@MockitoBean
	private ValidationServiceImpl validationService;
	
	@MockitoBean
	private HealthEndpoint healthEndpoint;
	
	@Nested
	@WebMvcTest(value = PolicyController.class)
	@TestPropertySource(properties = "web.mvc.bearer-token.required=false")
	class WithMockService {
		
		@Autowired
		private MockMvc mockMvc;
		
		@MockitoBean
		private AuthenticationProviderClient authenticationProviderClient;
		
		@MockitoBean
		private EDCConnectorClient edcConnectorClient;
		
		@MockitoBean
		private UsersRolesClient usersRolesClient;
		
		@MockitoBean
		private PolicyService policyService;
		
		@MockitoBean
	    private UsersRolesService usersRolesService;
		
		private static final String ACCESS_POLICY_REQ_PAYLOAD = """
			{
			    "resourceUri": "targetResourceUri",
			    "permissions" : [
			        {
			            "assignee": "consumer",
			            "action": "SEARCH",
			            "fromDatetime": "2024-08-01T00:00:00",
			            "toDatetime": "2024-08-31T23:59:59"
			        },
			        {
			            "assignee": "consumer",
			            "action": "CONSUME",
			            "fromDatetime": "2024-08-01T00:00:00",
			            "toDatetime2": "2024-09-30T23:59:59"
			        },
			        {
			            "assignee": "researcher",
			            "action": "RESTRICTED_CONSUME",
			            "fromDatetime": "2024-09-01T00:00:00",
			            "toDatetime": "2024-10-30T23:59:59"
			        }
			    ] 
			}
		""";
		
		private static final String USAGE_POLICY_REQ_PAYLOAD = """
			{
			    "resourceUri": "resourceUri",
			    "permissions" : [
			        {
			            "assignee": "consumer",
			            "constraints": [
			                {
			                    "type": "RestrictedNumber",
			                    "maxCount": 10
			                },
			                {
			                    "type": "RestrictedDuration",
			                    "fromDatetime": "2024-08-01T00:00:00",
			                    "toDatetime": "2024-08-31T23:59:59"
			                },
			                {
			                    "type": "Deletion"
			                }
			            ]
			        }
			    ]
			}
		""";
		
		@Test
	    void testGetIdentityAttributesOK() throws Exception {
	        
	        List<IdentityAttribute> identityAttributes = List.of(
	            new IdentityAttribute("attributeName1", "attributeCode1"),
	            new IdentityAttribute("attributeName2", "attributeCode2")
	        );

	        // Mock del metodo usersRoles.getIdentityAttributes
	        when(usersRolesService.getIdentityAttributes(VALID_JWT, "CONSUMER")).thenReturn(identityAttributes);

	        // Esecuzione e verifica della richiesta GET
	        mockMvc.perform(get("/api/policy/identity/attributes")
	                .header(HttpHeaders.AUTHORIZATION, AuthBearerUtil.toBearerString(VALID_JWT)))
	                .andExpect(status().isOk())
	                .andExpect(jsonPath("$[0].code").value("attributeCode1"))
	                .andExpect(jsonPath("$[0].identifier").value("attributeName1"))
	                .andExpect(jsonPath("$[1].code").value("attributeCode2"))
	                .andExpect(jsonPath("$[1].identifier").value("attributeName2"));
	    }
		
		@Test
		void testGetAccessPolicyActions() throws Exception {
			List<PolicyAction> actions = Arrays.asList(
				new PolicyAction("action1", "action1"),
				new PolicyAction("action2", "action2")
			);
			
			Mockito.when(policyService.getAccessPolicyActions())
				.thenReturn(actions)
			;

			mockMvc.perform(get("/api/policy/access/actions")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$[0].value").value("action1"))
				.andExpect(jsonPath("$[1].value").value("action2"))
				.andDo(print())
			;
		}

		@Test
		void testGetAccessPolicyJsonLD() throws Exception {
			OdrlPolicy policy = new OdrlPolicy("uid_0001", "target_0001");
			
			Mockito.when(policyService.getAccessPolicy(any(AccessPolicyRequest.class)))
				.thenReturn(policy)
			;

			mockMvc.perform(post("/api/policy/access")
					.header(HttpHeaders.AUTHORIZATION, AuthBearerUtil.toBearerString(VALID_JWT))
					.contentType(MediaType.APPLICATION_JSON)
					.content(ACCESS_POLICY_REQ_PAYLOAD)
					.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isOk())
			;
		}
		
		@Test
		void testGetUsagePolicyJsonLD() throws Exception {
			OdrlPolicy policy = new OdrlPolicy("uid_0001", "target_0001");
			
			Mockito.when(policyService.getUsagePolicy(any(UsagePolicyRequest.class)))
				.thenReturn(policy)
			;
				
			mockMvc.perform(post("/api/policy/usage")
					.header(HttpHeaders.AUTHORIZATION, AuthBearerUtil.toBearerString(VALID_JWT))
					.contentType(MediaType.APPLICATION_JSON)
					.content(USAGE_POLICY_REQ_PAYLOAD)
					.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isOk())
			;
		}
		
	}

	@Nested
	@WebMvcTest(value = PolicyController.class)
	@TestPropertySource(properties = "web.mvc.bearer-token.required=false")
	class WithoutMockService {
		
		@Autowired
		private MockMvc mockMvc;
		
		@MockitoBean
		private AuthenticationProviderClient authenticationProviderClient;
		
		@MockitoBean
		private EDCConnectorClient edcConnectorClient;
		
		@MockitoBean
		private UsersRolesClient usersRolesClient;
		
		@InjectMocks
		private UsersRolesServiceImpl usersRolesService;
		
		@Test
		void testGetAccessPolicyJsonLDWithNoDates() throws Exception {
			String content = """
				{
				    "resourceUri": "targetResourceUri",
				    "permissions" : [
				        {
				            "assignee": "consumer",
				            "action": "SEARCH"
				        },
				        {
				            "assignee": "consumer",
				            "action": "CONSUME"
				        },
				        {
				            "assignee": "researcher",
				            "action": "RESTRICTED_CONSUME"
				        }
				    ] 
				}
			""";

			mockMvc.perform(post("/api/policy/access")
					.header(HttpHeaders.AUTHORIZATION, AuthBearerUtil.toBearerString(VALID_JWT))
					.contentType(MediaType.APPLICATION_JSON)
					.content(content)
					.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isOk())
				//.andExpect(jsonPath("$.uid").value("uid_0001"))
				//.andExpect(jsonPath("$.target").value("target_0001"))
				.andDo(print())
			;
		}
		
		@Test
		void testGetUsagePolicyJsonLDWithNoDates() throws Exception {
			String content = """
				{
				    "resourceUri": "resourceUri",
				    "permissions" : [
				        {
				            "assignee": "consumer",
				            "constraints": [
				                {
				                    "type": "RestrictedNumber",
				                    "maxCount": 10
				                },
				                {
				                    "type": "RestrictedDuration"
				                },
				                {
				                    "type": "Deletion"
				                }
				            ]
				        }
				    ]
				}
			""";
				
			mockMvc.perform(post("/api/policy/usage")
					.header(HttpHeaders.AUTHORIZATION, AuthBearerUtil.toBearerString(VALID_JWT))
					.contentType(MediaType.APPLICATION_JSON)
					.content(content)
					.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(status().isOk())
				//.andExpect(jsonPath("$.uid").value("uid_0001"))
				//.andExpect(jsonPath("$.target").value("target_0001"))
				.andDo(print())
			;
		}	
	
		@Test
		void testGetIdentityAttributesWithNoBearer() throws Exception {
			mockMvc.perform(
				get("/api/policy/identity/attributes")
					.accept(MediaType.APPLICATION_JSON)
				)
				.andExpect(jsonPath("$").isEmpty())
			;
		}

	}
	
}
