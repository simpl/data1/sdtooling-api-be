package eu.europa.ec.simpl.sdtoolingbe.enumeration;

public enum OfferType {
    APPLICATION,
    DATA,
    INFRASTRUCTURE
}
