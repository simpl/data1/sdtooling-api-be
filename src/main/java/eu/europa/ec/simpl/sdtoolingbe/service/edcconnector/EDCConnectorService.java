package eu.europa.ec.simpl.sdtoolingbe.service.edcconnector;

import com.fasterxml.jackson.core.JsonProcessingException;

import eu.europa.ec.simpl.sdtoolingbe.exception.EDCRegistrationException;
import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;

public interface EDCConnectorService {

    String ASSET_URL_PATH = "$.{NS}generalServiceProperties.{NS}serviceAccessPoint.@value";
    String ASSET_DATA_ADRESS_JSON_PATH = "$.{NS}{propertiesName}.{NS}providerDataAddress";
    String ACCESS_POLICY_PATH = "$.{NS}servicePolicy.{NS}access-policy";
    String USAGE_POLICY_PATH = "$.{NS}servicePolicy.{NS}usage-policy";
    String INFRA_DEPLOYMENT_SCRIPT_ID_PATH = "$.{NS}infrastructureProperties.{NS}deploymentScriptId";
    String INFRA_PROVISIONING_API_PATH = "$.{NS}infrastructureProperties.{NS}provisionerApi";

    /**
     * @param sdJsonLd  Json-Ld
     * @param ecosystem Ecosystem
     * @return Json-Ld enriched
     * @throws InvalidSDJsonException Exception
     */
    String enrich(String sdJsonLd, String ecosystem) throws InvalidSDJsonException;

    /**
     * @param sdJsonLd      Json-Ld
     * @param ecosystem     Ecosystem
     * @param shapeFileName Shape fileName
     * @return String registered
     * @throws InvalidSDJsonException   Exception
     * @throws EDCRegistrationException Exception
     * @throws JsonProcessingException  Exception
     */
    String register(String sdJsonLd, String ecosystem, String shapeFileName)
        throws InvalidSDJsonException, EDCRegistrationException, JsonProcessingException;

}
