package eu.europa.ec.simpl.sdtoolingbe.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoInteractions;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

class WebMvcConfigTests {

    private WebMvcConfig webMvcConfig;
    
    private CorsRegistry corsRegistry;
    private CorsRegistry mockCorsRegistry;

    @BeforeEach
    void setUp() {
        // Initialize the WebMvcConfig instance and CorsRegistry mock
        webMvcConfig = new WebMvcConfig();
        
        corsRegistry = new CorsRegistry();
        mockCorsRegistry = mock(CorsRegistry.class);

        // Set the allowedOrigins property using ReflectionTestUtils
        ReflectionTestUtils.setField(webMvcConfig, "allowedOrigins", Arrays.asList("http://localhost", "http://example.com"));
    }

    @SuppressWarnings("unchecked")
	@Test
    void testAddCorsMappingsWithValidAllowedOrigins() {
    	
        webMvcConfig.addCorsMappings(corsRegistry);
        
        List<CorsRegistration> registrations = (List<CorsRegistration>) ReflectionTestUtils.getField(corsRegistry, "registrations");
        
        assertFalse(registrations.isEmpty());
        
        CorsRegistration registration = registrations.get(0);
        assertEquals("/**", ReflectionTestUtils.getField(registration, "pathPattern"));
        
        CorsConfiguration config = (CorsConfiguration) ReflectionTestUtils.getField(registration, "config");
        assertFalse(config.getAllowedHeaders().isEmpty());
        assertEquals("*", config.getAllowedHeaders().get(0));
        assertFalse(config.getAllowedMethods().isEmpty());
        assertEquals("*", config.getAllowedMethods().get(0));
        
        List<String> allowedOrigins = config.getAllowedOrigins();
        assertFalse(allowedOrigins.isEmpty());
        assertTrue(allowedOrigins.contains("http://localhost"));
        assertTrue(allowedOrigins.contains("http://example.com"));
    }

    @Test
    void testAddCorsMappingsNoAllowedOrigins() {
        // Set the allowedOrigins to an empty list
        ReflectionTestUtils.setField(webMvcConfig, "allowedOrigins", List.of());

        // Execute
        webMvcConfig.addCorsMappings(mockCorsRegistry);

        // Verify that no interaction with the registry happened
        verifyNoInteractions(mockCorsRegistry);
    }

    @Test
    void testAddCorsMappingsWithNullAllowedOrigins() {
        // Set the allowedOrigins to null
        ReflectionTestUtils.setField(webMvcConfig, "allowedOrigins", null);

        // Execute
        webMvcConfig.addCorsMappings(mockCorsRegistry);

        // Verify that no interaction with the registry happened
        verifyNoInteractions(mockCorsRegistry);
    }
}
