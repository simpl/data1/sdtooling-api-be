package eu.europa.ec.simpl.sdtoolingbe.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

public final class DateTimeUtil {

    private static final String GMT_ZONE_ID = "GMT";
    private static final String UTC_ZONE_ID = "UTC";

    private DateTimeUtil() {
    }

    /**
     * @param dateTime
     * @param zoneId   (optional: null, default 'GMT')
     * @return
     */
    public static String dateTimeToString(Date dateTime, ZoneId zoneId) {
        if (zoneId == null) {
            zoneId = ZoneId.of(GMT_ZONE_ID);
        }
        ZonedDateTime zoneDateTime = ZonedDateTime.of(
            LocalDateTime.ofInstant(dateTime.toInstant(), zoneId),
            zoneId
        );

        DateTimeFormatter formatter;
        // ISO format with the timezone: 2011-12-03T10:15:30+01:00
        formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

        // // ISO format without the timezone: 2011-12-03T10:15:30
        // formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

        return zoneDateTime.format(formatter);
    }

    public static Date dateTimeFromString(String dateTimeAsString) {
        /*
         * accetta i formati con o senza il timezone (senza assume GMT): '2011-12-03T10:15:30' '2011-12-03T10:15:30Z'
         * '2011-12-03T10:15:30+01:00' '2011-12-03T10:15:30+01:00[Europe/Paris]'
         */
        Instant instant = new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .appendPattern("yyyy-MM-dd'T'HH:mm:ss[Z][z]")
            .toFormatter()
            .withZone(ZoneId.of(UTC_ZONE_ID))
            .parse(dateTimeAsString, Instant::from);
        return Date.from(instant);
    }

}
