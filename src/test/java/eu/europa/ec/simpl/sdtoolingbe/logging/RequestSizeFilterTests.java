package eu.europa.ec.simpl.sdtoolingbe.logging;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

class RequestSizeFilterTests {

    private RequestSizeFilter filter;

    @Mock
    private HttpServletRequest mockRequest;
    @Mock
    private HttpServletResponse mockResponse;
    @Mock
    private FilterChain mockChain;

    @BeforeEach
    public void beforeEach() {
        MockitoAnnotations.openMocks(this);
        filter = new RequestSizeFilter();
    }

    @Test
    void testDoFilter() throws IOException, ServletException {
        
        int contentLength = 12345;
        when(mockRequest.getContentLength()).thenReturn(contentLength);
         
        filter.doFilter(mockRequest, mockResponse, mockChain);
        
        verify(mockChain).doFilter(any(), any());
    }

}
