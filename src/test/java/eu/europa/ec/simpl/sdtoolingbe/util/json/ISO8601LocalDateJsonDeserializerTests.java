package eu.europa.ec.simpl.sdtoolingbe.util.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

class ISO8601LocalDateJsonDeserializerTests {
	
	@InjectMocks
    private ISO8601LocalDateJsonDeserializer deserializer;

    @Mock
    private JsonParser jsonParser;

    @Mock
    private DeserializationContext deserializationContext;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
    
    @Test
    void testDeserializeWithValidDate() throws IOException {
        String validDate = "2011-12-03T10:15:30";
        when(jsonParser.getText()).thenReturn(validDate);

        Date result = deserializer.deserialize(jsonParser, deserializationContext);

        LocalDateTime expectedDateTime = LocalDateTime.parse(validDate, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        Instant expectedInstant = ZonedDateTime.of(expectedDateTime, TimeZone.getDefault().toZoneId()).toInstant();
        assertEquals(Date.from(expectedInstant), result);
    }
    
    @Test
    void testDeserializeInvalidValidDateWithTimeZone() throws IOException {
        // Prepare
        String validDateWithoutTimeZone = "2011-12-03T10:15:30+01:00";
        when(jsonParser.getText()).thenReturn(validDateWithoutTimeZone);
        
        assertThrows(IOException.class, () -> deserializer.deserialize(jsonParser, deserializationContext));
    }

    @Test
    void testDeserializeWithBlankString() throws IOException {
        when(jsonParser.getText()).thenReturn("   ");

        Date result = deserializer.deserialize(jsonParser, deserializationContext);

        assertNull(result);
    }

    @Test
    void testDeserializeInvalidDate() throws IOException {
        String invalidDate = "invalid-date";
        when(jsonParser.getText()).thenReturn(invalidDate);

        assertThrows(IOException.class, () -> deserializer.deserialize(jsonParser, deserializationContext));
    }
	
}
