package eu.europa.ec.simpl.sdtoolingbe.util.security;

import java.io.IOException;
import java.io.StringReader;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.List;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMParser;

public final class SecurityUtil {

    private SecurityUtil() {
    }

    /**
     * 
     * @param privateKey
     * @param pemContent
     * @param keyStorePassword (optional: null)
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public static KeyStore createPKCS12Keystore(PrivateKey privateKey, String pemContent, String keyStorePassword)
        throws GeneralSecurityException, IOException {
        return createPKCS12Keystore(
            privateKey,
            createX509Certificates(pemContent),
            keyStorePassword
        );
    }

    /**
     * 
     * @param privateKey
     * @param certificates
     * @param keyStorePassword (optional: null)
     * @return
     * @throws CertificateException
     * @throws KeyStoreException
     * @throws IOException
     */
    public static KeyStore createPKCS12Keystore(
        PrivateKey privateKey, List<Certificate> certificates, String keyStorePassword
    ) throws GeneralSecurityException, IOException {
        verifyPrivateKeyMatchesCertificateChain(privateKey, (X509Certificate) certificates.get(0));

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(null, null);

        char[] password = null;
        if(keyStorePassword != null) {
            password = keyStorePassword.toCharArray();
        }

        keyStore.setKeyEntry("alias", privateKey, password, certificates.toArray(new Certificate[certificates.size()]));
        return keyStore;
    }

    public static PrivateKey createPrivateKey(byte[] encoded, String algorithm) throws GeneralSecurityException {
        var keySpec = new PKCS8EncodedKeySpec(encoded);
        var keyFactory = KeyFactory.getInstance(algorithm);
        return keyFactory.generatePrivate(keySpec);
    }

    public static List<Certificate> createX509Certificates(String pem) throws GeneralSecurityException, IOException {
        List<Certificate> certificates = new ArrayList<>();
        JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();

        try (PEMParser pemParser = new PEMParser(new StringReader(pem))) {
            Object object;
            while ((object = pemParser.readObject()) != null) {
                if (object instanceof X509CertificateHolder certHolder) {
                    X509Certificate certificate = certificateConverter.getCertificate(certHolder);
                    certificates.add(certificate);
                }
            }
        }
        return certificates;
    }

    public static void verifyPrivateKeyMatchesCertificateChain(PrivateKey privateKey, X509Certificate certificate)
        throws GeneralSecurityException {
        byte[] testData = "test".getBytes();
        Signature signature = Signature.getInstance(certificate.getSigAlgName());
        signature.initSign(privateKey);
        signature.update(testData);
        byte[] signedData = signature.sign();

        signature.initVerify(certificate.getPublicKey());

        signature.update(testData);
        if (!signature.verify(signedData)) {
            throw new GeneralSecurityException("private key doesn't match the certificate chain");
        }

    }

}
