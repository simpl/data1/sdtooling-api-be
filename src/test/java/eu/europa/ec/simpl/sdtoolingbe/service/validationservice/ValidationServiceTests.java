package eu.europa.ec.simpl.sdtoolingbe.service.validationservice;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.client.ValidationClient;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.properties.ValidationProperties;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationServiceImpl;
import eu.europa.ec.simpl.sdtoolingbe.util.ShaclFileUtil;
import feign.FeignException;
import feign.Request;
import feign.RetryableException;

class ValidationServiceTests {

    private static final String TEST_FILE = "test/frontend-NEW-data-offering.json";

    @Mock
    private ValidationClient validationClient;

    @Mock
    private ValidationProperties validationProperties;

    @Mock
    private static Request feignRequest;

    @InjectMocks
    private ValidationServiceImpl validationService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        when(validationProperties.isEnabled()).thenReturn(true);
        when(validationProperties.getValidationUrl()).thenReturn("http://example.com");
        when(validationProperties.getValidationJsonldUrl()).thenReturn("http://example.com");
    }

    @BeforeAll
    static void staticSetUp() {
        feignRequest = Mockito.mock(Request.class);
    }

    private static Stream<Arguments> GenerateHashFromJsonSuccessAndCustomParameters() {
        String validationApiResponseSuccess = "{ \"@graph\": [] }";
        String validationApiResponseCustomException = "{ \"@graph\": [ { \"sh:value\": {\"@value\": \"Test\"}, \"sh:resultMessage\": \"Error\", \"sh:resultPath\": {\"@id\": \"ecosystem:field\"} } ] }";

        return Stream.of(Arguments.of(validationApiResponseSuccess, null), Arguments.of(validationApiResponseCustomException, CustomException.class));
    }

    @ParameterizedTest
    @MethodSource("GenerateHashFromJsonSuccessAndCustomParameters")
    <T extends Throwable> void testValidateJsonLdSuccessAndCustom(String validationApiResponse, Class<T> exceptionClass) throws Exception {

        ResponseEntity<String> responseEntity = new ResponseEntity<>(validationApiResponse, HttpStatus.OK);

        when(validationClient.validateJsonLd(any(URI.class), any(MultipartFile.class), any(MultipartFile.class), anyString())).thenReturn(responseEntity);

        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> {
                try (InputStream contentStream = TestSupport.getResourceAsStream(TEST_FILE)) {
                    assert contentStream != null;
                    validationService.validateJsonLd(contentStream.toString(), "data-offeringShape.ttl", "ecosystem");
                }
            }, "Expected exception of type " + exceptionClass.getName() + " was not thrown during the validation process.");
        } else {
            try (InputStream contentStream = TestSupport.getResourceAsStream(TEST_FILE)) {
                assert contentStream != null;
                validationService.validateJsonLd(contentStream.toString(), "data-offeringShape.ttl", "ecosystem");
            }
        }

        verify(validationClient).validateJsonLd(any(URI.class), any(MultipartFile.class), any(MultipartFile.class), anyString());

    }


    private static Stream<Arguments> GenerateHashFromJsonResponseEntityParameters() {
        return Stream.of(
                Arguments.of(new FeignException.BadRequest("Test exception", feignRequest, null, null)),
                Arguments.of(new RetryableException(HttpStatus.BAD_REQUEST.value(), "Test exception", null, 0L, feignRequest))
        );
    }

    @ParameterizedTest
    @MethodSource("GenerateHashFromJsonResponseEntityParameters")
    void testValidateJsonLdException(Throwable exception) throws Exception {

        when(validationClient.validateJsonLd(any(URI.class), any(MultipartFile.class), any(MultipartFile.class), anyString())).thenThrow(exception);

        try (InputStream contentStream = TestSupport.getResourceAsStream(TEST_FILE)) {
            assert contentStream != null;
            validationService.validateJsonLd(contentStream.toString(), "data-offeringShape.ttl", "ecosystem");
        }

        verify(validationClient).validateJsonLd(any(URI.class), any(MultipartFile.class), any(MultipartFile.class), anyString());

    }

    @Test
    void testValidateJsonProcessingException() throws Exception {
        try (InputStream contentStream = TestSupport.getResourceAsStream(TEST_FILE)) {
            assert contentStream != null;
            when(validationClient.validateJsonLd(any(URI.class), any(MultipartFile.class), any(MultipartFile.class), anyString())).thenReturn(new ResponseEntity<>("test json", HttpStatus.OK));
            validationService.validateJsonLd(contentStream.toString(), "data-offeringShape.ttl", "ecosystem");
        }
    }

    @Test
    void testValidateIOException() throws Exception {

        try (MockedStatic<ShaclFileUtil> mockedStatic = mockStatic(ShaclFileUtil.class)) {
            mockedStatic.when(() -> ShaclFileUtil.getTtlFromFilename(anyString(), any())).thenThrow(new IOException("Test IOException"));
            try (InputStream contentStream = TestSupport.getResourceAsStream(TEST_FILE)) {
                assert contentStream != null;
                validationService.validateJsonLd(contentStream.toString(), "data-offeringShape.ttl", "ecosystem");
            }
        }
    }


    @Test
    void testValidationNotEnabled() throws IOException {

        when(validationProperties.isEnabled()).thenReturn(false);

        try (InputStream contentStream = TestSupport.getResourceAsStream(TEST_FILE)) {
            assert contentStream != null;
            validationService.validateJsonLd(contentStream.toString(), "data-offeringShape.ttl", "ecosystem");
        }

    }

}
