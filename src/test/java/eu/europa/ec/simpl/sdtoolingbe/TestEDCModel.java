package eu.europa.ec.simpl.sdtoolingbe;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data.IonosS3DataAddress;

public class TestEDCModel {
	
	protected static ObjectMapper om = new ObjectMapper();
	
	public static void main(String[] args) throws Exception {
		
		String bucketName = "simpl-provider";
		String blobName = "test-document.txt";
		
		IonosS3DataAddress dataAddress = new IonosS3DataAddress(
			"de", "s3-eu-central-1.ionoscloud.com", bucketName, 
			blobName, "test-key-name"
		);
		dataAddress.setAccessKey("EAA...jZz");
		dataAddress.setSecretKey("6Mo..UHs");
		
		String json = om.writeValueAsString(dataAddress);
		System.out.println(json);
		
		String escapedJson = JSONObject.quote(json);
		System.out.println(escapedJson);
		
	}

}
