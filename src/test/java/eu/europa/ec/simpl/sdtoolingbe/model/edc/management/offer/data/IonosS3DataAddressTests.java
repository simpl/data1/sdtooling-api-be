package eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class IonosS3DataAddressTests {
    
    @Test
    void testDefaultConstructor() {
        IonosS3DataAddress address = new IonosS3DataAddress();
        assertNull(address.getRegion());
        assertNull(address.getStorage());
        assertNull(address.getBucketName());
        assertNull(address.getBlobName());
        assertNull(address.getKeyName());
        assertNull(address.getAccessKey());
        assertNull(address.getSecretKey());
    }

    
    @Test
    void testParameterizedConstructor() {
        IonosS3DataAddress address = new IonosS3DataAddress("de", "s3-eu-central-1.ionoscloud.com", 
                                                           "simpl-provider", "test-document-2.txt", 
                                                           "test-key-name");
        assertEquals("de", address.getRegion());
        assertEquals("s3-eu-central-1.ionoscloud.com", address.getStorage());
        assertEquals("simpl-provider", address.getBucketName());
        assertEquals("test-document-2.txt", address.getBlobName());
        assertEquals("test-key-name", address.getKeyName());
    }

    @Test
    void testEqualsAndHashCode() {
        IonosS3DataAddress address1 = new IonosS3DataAddress("de", "s3-eu-central-1.ionoscloud.com", 
                                                            "simpl-provider", "test-document-2.txt", 
                                                            "test-key-name");
        IonosS3DataAddress address2 = new IonosS3DataAddress("de", "s3-eu-central-1.ionoscloud.com", 
                                                            "simpl-provider", "test-document-2.txt", 
                                                            "test-key-name");

        assertEquals(address1, address2);
        assertEquals(address1.hashCode(), address2.hashCode());
    }

    @Test
    void testNotEquals() {
        IonosS3DataAddress address1 = new IonosS3DataAddress("de", "s3-eu-central-1.ionoscloud.com", 
                                                            "simpl-provider", "test-document-2.txt", 
                                                            "test-key-name");
        IonosS3DataAddress address2 = new IonosS3DataAddress("us", "s3-eu-central-1.ionoscloud.com", 
                                                            "simpl-provider", "test-document-2.txt", 
                                                            "test-key-name");

        assertNotEquals(address1, address2);
    }

    @Test
    void testSuperClassTypeName() {
        IonosS3DataAddress address = new IonosS3DataAddress("de", "s3-eu-central-1.ionoscloud.com", 
                                                           "simpl-provider", "test-document-2.txt", 
                                                           "test-key-name");

        assertEquals("IonosS3", address.getType());
    }
    
    @Test
    void testSetters() {
        IonosS3DataAddress address = new IonosS3DataAddress();

        address.setRegion("de");
        address.setStorage("s3-eu-central-1.ionoscloud.com");
        address.setBucketName("simpl-provider");
        address.setBlobName("test-document-2.txt");
        address.setKeyName("test-key-name");
        address.setAccessKey("EAA...jZz");
        address.setSecretKey("6Mo..UHs");

        assertEquals("de", address.getRegion());
        assertEquals("s3-eu-central-1.ionoscloud.com", address.getStorage());
        assertEquals("simpl-provider", address.getBucketName());
        assertEquals("test-document-2.txt", address.getBlobName());
        assertEquals("test-key-name", address.getKeyName());
        assertEquals("EAA...jZz", address.getAccessKey());
        assertEquals("6Mo..UHs", address.getSecretKey());
    }
    
    @Test
    void testToString() {
        IonosS3DataAddress address = new IonosS3DataAddress("de", "s3-eu-central-1.ionoscloud.com", 
                                                           "simpl-provider", "test-document-2.txt", 
                                                           "test-key-name");
        String expectedToString = "IonosS3DataAddress(region=de, storage=s3-eu-central-1.ionoscloud.com, " +
                                  "bucketName=simpl-provider, blobName=test-document-2.txt, " +
                                  "keyName=test-key-name, accessKey=null, secretKey=null)";

        assertEquals(expectedToString, address.toString());
    }
}

