package eu.europa.ec.simpl.sdtoolingbe.util.json;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Jackson JSON serializer che converte una Date in una stringa in formato ISO8601 con il timezone.
 * 
 * @author apadula
 */
public class ISO8601OffsetDateJsonSerializer extends JsonSerializer<Date> {

    @Override
    public void serialize(Date value, JsonGenerator generator, SerializerProvider serializer)
        throws IOException {
        if (value != null) {
            ZonedDateTime datetime = ZonedDateTime.of(
                LocalDateTime.ofInstant(value.toInstant(), ZoneId.systemDefault()),
                ZoneId.systemDefault()
            );

            /*
             * produce il formato 2023-02-28T15:39:00.137+01:00
             */
            String dateAsString = datetime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            generator.writeString(dateAsString);
        } else {
            generator.writeNull();
        }
    }

}
