package eu.europa.ec.simpl.sdtoolingbe.client.edcconnector;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.AssetDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.ContractDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.PolicyDefinition;

@FeignClient(value = "edcConnectorClient", url = "${edc-connector.base-url}")
public interface EDCConnectorClient {

    String EDC_MANAGEMENT_PATH = "/management/v3";

    @PostMapping(value = EDC_MANAGEMENT_PATH + "/assets", consumes = "application/json", produces = "application/json")
    ResponseEntity<String> registerAsset(
        @RequestBody String body
    );

    @PostMapping(value = EDC_MANAGEMENT_PATH + "/assets", consumes = "application/json", produces = "application/json")
    ResponseEntity<String> register(
        @RequestBody AssetDefinition asset
    );

    @PostMapping(
        value = EDC_MANAGEMENT_PATH + "/policydefinitions", consumes = "application/json", produces = "application/json"
    )
    ResponseEntity<String> registerPolicyDefinition(
        @RequestBody String body
    );

    @PostMapping(
        value = EDC_MANAGEMENT_PATH + "/policydefinitions", consumes = "application/json", produces = "application/json"
    )
    ResponseEntity<String> register(
        @RequestBody PolicyDefinition policyDefinition
    );

    @PostMapping(
        value = EDC_MANAGEMENT_PATH
            + "/contractdefinitions", consumes = "application/json", produces = "application/json"
    )
    ResponseEntity<String> registerContractDefinition(
        @RequestBody String body
    );

    @PostMapping(
        value = EDC_MANAGEMENT_PATH
            + "/contractdefinitions", consumes = "application/json", produces = "application/json"
    )
    ResponseEntity<String> register(
        @RequestBody ContractDefinition contractDefinition
    );

}
