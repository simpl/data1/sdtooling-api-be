package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PolicyDefinition extends AbstractManagement {
    /*
     * { "@context": { "@vocab": "https://w3id.org/edc/v0.0.1/ns/" }, "policy": {...} }
     */

    private final OdrlPolicy policy;

}
