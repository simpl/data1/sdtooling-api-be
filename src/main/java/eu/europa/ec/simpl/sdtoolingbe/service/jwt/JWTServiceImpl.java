package eu.europa.ec.simpl.sdtoolingbe.service.jwt;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTVerificationException;

import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class JWTServiceImpl implements JWTService {

    @Override
    public void checkJWT(String token) throws UnauthorizedException {
        log.debug("checkJWT() for token {}", token);

        try {
            // al momento solo una validazione well formed
            JWT.decode(token);
        } catch (JWTVerificationException e) {
            log.debug("checkJWT() failed for token {}", token, e);
            throw new UnauthorizedException("invalid token");
        }
    }

}
