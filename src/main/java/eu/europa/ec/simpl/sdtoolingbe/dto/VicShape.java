package eu.europa.ec.simpl.sdtoolingbe.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VicShape {
    private final String schema;
    private final String targetClassPrefix;
    private final String targetClassName;
    private final List<ShapeProperties> constraints;

    public VicShape(
        List<ShapeProperties> constraints, String schema, String targetClassPrefix, String targetClassName
    ) {
        this.targetClassPrefix = targetClassPrefix;

        if (constraints == null) {
            this.constraints = new ArrayList<>();
        } else {
            this.constraints = new ArrayList<>(constraints);
        }

        this.schema = schema;
        this.targetClassName = targetClassName;
    }

    public String getSchema() {
        return schema;
    }

    public String getTargetClassPrefix() {
        return targetClassPrefix;
    }

    public String getTargetClassName() {
        return targetClassName;
    }

    public List<ShapeProperties> getConstraints() {
        return Collections.unmodifiableList(constraints);
    }

}
