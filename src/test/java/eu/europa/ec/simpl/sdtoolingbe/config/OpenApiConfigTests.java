package eu.europa.ec.simpl.sdtoolingbe.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import io.swagger.v3.oas.models.OpenAPI;

@ExtendWith(MockitoExtension.class)
class OpenApiConfigTests {

    private OpenApiConfig openApiConfig = new OpenApiConfig();

    @Test
    void testWithServers() {
        ReflectionTestUtils.setField(openApiConfig, "servers", List.of("https://api.example.com"));
        
        OpenAPI openAPI = openApiConfig.openAPI();

        assertNotNull(openAPI);
        assertNotNull(openAPI.getInfo());
        assertNotNull(openAPI.getSecurity());
        assertFalse(openAPI.getServers().isEmpty());
        assertEquals("https://api.example.com", openAPI.getServers().get(0).getUrl());
    }

}

