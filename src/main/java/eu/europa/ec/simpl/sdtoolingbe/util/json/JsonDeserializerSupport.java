package eu.europa.ec.simpl.sdtoolingbe.util.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Supporto per deserializzatori JSON
 * 
 * @author apadula
 */
public abstract class JsonDeserializerSupport<T> extends JsonDeserializer<T> {

    protected JsonDeserializerException newUnparsableDateTimeException(JsonParser parser) throws IOException {
        String fieldName = parser.currentName();
        return new JsonDeserializerException(fieldName + " format is invalid");
    }

}
