package eu.europa.ec.simpl.sdtoolingbe.error;

import org.apache.commons.lang3.StringUtils;

import lombok.ToString;

@ToString
public class ErrorResponse {

    private final String errorTitle;
    private final String errorDescription;

    public ErrorResponse(String errorTitle, String errorDescription) {
        this.errorTitle = errorTitle;
        this.errorDescription = errorDescription;
    }

    public ErrorResponse(String errorDescription) {
        this(null, errorDescription);
    }

    public String getErrorTitle() {
        return StringUtils.defaultIfBlank(errorTitle, "Generic error");
    }

    public String getErrorDescription() {
        return errorDescription;
    }

}
