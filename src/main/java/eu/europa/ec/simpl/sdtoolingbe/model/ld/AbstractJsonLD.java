package eu.europa.ec.simpl.sdtoolingbe.model.ld;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class AbstractJsonLD {

    @JsonProperty("uid")
    private String uid;

    @JsonProperty("@context")
    private String context;

    @JsonProperty("@type")
    private String type;

    private String profile;

    protected AbstractJsonLD(String uid, String context) {
        this.uid = uid;
        this.context = context;
    }

}
