package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import java.util.HashMap;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AssetDefinition extends AbstractManagement {
    
    private DataAddress dataAddress;
    /*
     * { "@context": { "@vocab": "https://w3id.org/edc/v0.0.1/ns/" }, "properties": { "name": "product description",
     * "contenttype": "application/json" }, "dataAddress": { "type": "HttpData", "baseUrl":
     * "https://sd-ui.dev.simpl-europe.eu" } }
     */
    public AssetDefinition() {
        setProperties(new HashMap<>());
    }

    

}
