package eu.europa.ec.simpl.sdtoolingbe.util;

public final class ExceptionUtil {

    private ExceptionUtil() {
    }

    public static <T extends Throwable> T findCause(Throwable t, Class<T> clazz) {
        Throwable cause = t.getCause();
        while (cause != null) {
            if (cause.getClass().isAssignableFrom(clazz)) {
                return (T) cause;
            }
            cause = cause.getCause();
        }
        return null;
    }

}
