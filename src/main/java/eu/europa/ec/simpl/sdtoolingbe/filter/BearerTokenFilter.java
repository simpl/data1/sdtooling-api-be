package eu.europa.ec.simpl.sdtoolingbe.filter;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.service.jwt.JWTService;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class BearerTokenFilter extends HttpFilter {

    @Value("${web.mvc.bearer-token.required}")
    private boolean bearerTokenRequired;

    @Value("${web.mvc.bearer-token.allowed-paths}")
    private List<String> bearerTokenAllowedPaths;

    @Value("${springdoc.api-docs.path}")
    private String apiDocsPath;

    @Value("${springdoc.swagger-ui.path}")
    private String swaggerUiPath;

    private transient JWTService jwtService;

    private transient AntPathMatcher antPathMatcher;

    public BearerTokenFilter(JWTService jwtService) {
        this.jwtService = jwtService;
        this.antPathMatcher = new AntPathMatcher();
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (bearerTokenRequired && !isPathAllowed(request)) {
            String jwtToken = AuthBearerUtil.getBearerValue(request);
            if (jwtToken == null) {
                log.error("doFilter(): no Bearer value found in Authorization header");
                // Rispondi con 401 Unauthorized se non presente o non valido
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().write("Missing or invalid Authorization header");
                return;
            }

            try {
                jwtService.checkJWT(jwtToken);
            } catch (UnauthorizedException e) {
                log.error("doFilter(): invalid Bearer value found in Authorization header: {}", e);
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.getWriter().write("Invalid Authorization header value");
                return;
            }
        }

        chain.doFilter(request, response);
    }

    private boolean isPathAllowed(HttpServletRequest request) {
        String requestUri = request.getRequestURI();
        if (requestUri == null) {
            return false;
        }
        return checkAllowedPaths(requestUri) || checkDocPaths(requestUri);
    }

    private boolean checkAllowedPaths(String requestUri) {
        if (bearerTokenAllowedPaths == null || bearerTokenAllowedPaths.isEmpty()) {
            return false;
        }
        return bearerTokenAllowedPaths.stream().anyMatch(pattern -> antPathMatcher.match(pattern, requestUri));
    }

    private boolean checkDocPaths(String requestUri) {
        return requestUri.startsWith(swaggerUiPath) || requestUri.startsWith(apiDocsPath);
    }
}
