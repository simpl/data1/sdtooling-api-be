package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jayway.jsonpath.JsonPath;

import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data.HttpDataDataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.data.IonosS3DataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.infrastructure.InfrastructureDataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;

class EDCManagementModelTests {
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	public EDCManagementModelTests() {
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
	}

	@Test
	void testAssetDefinitionModelWithHttpData() throws Exception {
		
		AssetDefinition assetDefinition = new AssetDefinition();
		HttpDataDataAddress dataAddress = new HttpDataDataAddress("http://data.endpoint.site.com");
		assetDefinition.setDataAddress(dataAddress);
		
		assetDefinition.setProperty("prop1", "value1");
		
		String json = objectMapper.writeValueAsString(assetDefinition);
		
		System.out.println("\n##### AssetDefinition #####");
		System.out.println(json);
		
		assertNotNull(json);
		assertEquals("https://w3id.org/edc/v0.0.1/ns/", JsonPath.read(json, "$.@context.@vocab" ));
		
		assertEquals(dataAddress.getType(), JsonPath.read(json, "$.dataAddress.type" ));
		assertEquals(dataAddress.getBaseUrl(), JsonPath.read(json, "$.dataAddress.baseUrl" ));
		
		assertEquals("value1", JsonPath.read(json, "$.properties.prop1" ));
	}
	
	@Test
	void testAssetDefinitionModelWithInfrastructureData() throws Exception {
		
		AssetDefinition assetDefinition = new AssetDefinition();
		InfrastructureDataAddress dataAddress = new InfrastructureDataAddress(
			"http://provisioning.api.site.com", UUID.randomUUID().toString()
		);
		assetDefinition.setDataAddress(dataAddress);
		
		assetDefinition.setProperty("prop1", "value1");
		
		String json = objectMapper.writeValueAsString(assetDefinition);
		
		System.out.println("\n##### AssetDefinition #####");
		System.out.println(json);
		
		assertNotNull(json);
		assertEquals("https://w3id.org/edc/v0.0.1/ns/", JsonPath.read(json, "$.@context.@vocab" ));
		
		assertEquals(dataAddress.getType(), JsonPath.read(json, "$.dataAddress.type" ));
		assertEquals(dataAddress.getDeploymentScriptId(), JsonPath.read(json, "$.dataAddress.deploymentScriptId" ));
		assertEquals(dataAddress.getProvisioningAPI(), JsonPath.read(json, "$.dataAddress.provisioningAPI" ));
		
		assertEquals("value1", JsonPath.read(json, "$.properties.prop1" ));
		
	}
	
	@Test
	void testAssetDefinitionModelWithIonosS3Data() throws Exception {
		
		AssetDefinition assetDefinition = new AssetDefinition();
		IonosS3DataAddress dataAddress = new IonosS3DataAddress(
			"de", "storage", "bucketName", "blobName", "keyName" 
		);
		assetDefinition.setDataAddress(dataAddress);
		
		assetDefinition.setProperty("prop1", "value1");
		
		String json = objectMapper.writeValueAsString(assetDefinition);
		
		System.out.println("\n##### AssetDefinition #####");
		System.out.println(json);
		
		assertNotNull(json);
		assertEquals("https://w3id.org/edc/v0.0.1/ns/", JsonPath.read(json, "$.@context.@vocab" ));
		
		assertEquals(dataAddress.getType(), JsonPath.read(json, "$.dataAddress.type" ));
		assertEquals(dataAddress.getBlobName(), JsonPath.read(json, "$.dataAddress.blobName" ));
		assertEquals(dataAddress.getBucketName(), JsonPath.read(json, "$.dataAddress.bucketName" ));
		assertEquals(dataAddress.getKeyName(), JsonPath.read(json, "$.dataAddress.keyName" ));
		assertEquals(dataAddress.getRegion(), JsonPath.read(json, "$.dataAddress.region" ));
		assertEquals(dataAddress.getStorage(), JsonPath.read(json, "$.dataAddress.storage" ));
		
		assertEquals("value1", JsonPath.read(json, "$.properties.prop1" ));
		
	}
	
	@Test
	void testContractDefinitionModel() throws Exception {
		
		ContractDefinition policyDefinition = new ContractDefinition(
			"asset-id", "access-policy-id", "contract-policy-id"
		);
		
		String json = objectMapper.writeValueAsString(policyDefinition);
		
		System.out.println("\n##### ContractDefinition #####");
		System.out.println(json);
		
		assertNotNull(json);
		assertEquals("https://w3id.org/edc/v0.0.1/ns/", JsonPath.read(json, "$.@context.@vocab" ));
		
		assertEquals(policyDefinition.getAccessPolicyId(), JsonPath.read(json, "$.accessPolicyId" ));
		assertEquals(policyDefinition.getContractPolicyId(), JsonPath.read(json, "$.contractPolicyId" ));
		
		assertEquals(policyDefinition.getAssetsSelectors().get(0).getOperandRight(), JsonPath.read(json, "$.assetsSelector[0].operandRight" ));
	}
	
	@Test
	void testPolicyDefinitionModel() throws Exception {
		
		OdrlPolicy policy = new OdrlPolicy();
		PolicyDefinition policyDefinition = new PolicyDefinition(policy);
		
		
		String json = objectMapper.writeValueAsString(policyDefinition);
		
		System.out.println("\n##### PolicyDefinition #####");
		System.out.println(json);
		
		assertNotNull(json);
		assertEquals("https://w3id.org/edc/v0.0.1/ns/", JsonPath.read(json, "$.@context.@vocab" ));
		
	}
	
	

}
