package eu.europa.ec.simpl.sdtoolingbe.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Setter;

@Component
@PropertySource(value = "classpath:validation-config.yml", factory = YamlPropertySourceFactory.class)
public class ValidationProperties {

    @Setter
    @Value("${validation.config.enabled}")
    private boolean enabled;

    @Value("${validation.config.domain}")
    private String domain;

    @Value("${validation.config.validationapi}")
    private String validationapi;

    @Value("${validation.config.validation-jsonld-api}")
    private String validationJsonldApi;

    public String getValidationapi() {
        return validationapi;
    }

    public void setValidationapi(String validationapi) {
        this.validationapi = validationapi;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getValidationUrl() {
        return this.domain + this.validationapi;
    }

    public String getValidationJsonldUrl() {
        return this.domain + this.validationJsonldApi;
    }
}
