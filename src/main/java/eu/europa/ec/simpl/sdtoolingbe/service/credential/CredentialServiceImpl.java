package eu.europa.ec.simpl.sdtoolingbe.service.credential;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Base64;

import org.json.JSONObject;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider.AuthenticationProviderClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.util.security.SecurityUtil;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import feign.FeignException;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class CredentialServiceImpl implements CredentialService {

    private AuthenticationProviderClient authenticationProviderClient;
    private UsersRolesClient usersRolesClient;

    public CredentialServiceImpl(
        AuthenticationProviderClient authenticationProviderClient, UsersRolesClient usersRolesClient
    ) {
        this.authenticationProviderClient = authenticationProviderClient;
        this.usersRolesClient = usersRolesClient;
    }

    @Override
    @Cacheable()
    public KeyStore getCredential(String bearerToken) throws UnauthorizedException, KeyStoreBuildException {
        try {
            log.debug("getCredential(): invoking authenticationProviderClient.getKeypair()");
            String keypairJson = authenticationProviderClient.getKeypair(AuthBearerUtil.toBearerString(bearerToken));
            log.debug("getCredential(): invoking usersRolesClient.getCredential()");
            String pemContent = usersRolesClient.getCredential(AuthBearerUtil.toBearerString(bearerToken));

            // creating private key
            JSONObject keypairObj = new JSONObject(keypairJson);
            PrivateKey privateKey = SecurityUtil
                .createPrivateKey(Base64.getDecoder().decode(keypairObj.getString("privateKey")), "EC");

            // creating keystore
            return SecurityUtil.createPKCS12Keystore(
                privateKey, pemContent, null
            );
        } catch (FeignException.Unauthorized e) {
            log.error("getCredential() failed", e);
            throw new UnauthorizedException("invalid bearer token");
        } catch (Exception e) {
            log.error("getCredential() failed", e);
            throw new KeyStoreBuildException(e);
        }
    }

}
