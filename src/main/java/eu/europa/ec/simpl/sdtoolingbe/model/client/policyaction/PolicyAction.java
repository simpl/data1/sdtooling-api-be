package eu.europa.ec.simpl.sdtoolingbe.model.client.policyaction;

import lombok.Data;

@Data
public class PolicyAction {

    private final String label;
    private final String value;

}
