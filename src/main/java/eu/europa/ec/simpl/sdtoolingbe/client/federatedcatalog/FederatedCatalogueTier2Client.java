package eu.europa.ec.simpl.sdtoolingbe.client.federatedcatalog;

import java.util.Map;

import org.springframework.http.MediaType;

import eu.europa.ec.simpl.client.feign.annotations.PreflightEphemeralProof;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface FederatedCatalogueTier2Client {

    @RequestLine("POST {pathPrefix}/self-descriptions")
    @Headers(
        {
            "Content-Type: " + MediaType.APPLICATION_JSON_VALUE
        }
    )
    @PreflightEphemeralProof
    /**
     * The type Map<String, Object> is necessary to have a plain json string in the request body using this client
     * 
     * @param sdJsonLdObj
     * @return the response json as Map<String, Object> (necessary to get the json from the response using this client)
     */
    Map<String, Object> selfDescriptions(
        @Param String pathPrefix,
        Map<String, Object> sdJsonLdObj
    );

}
