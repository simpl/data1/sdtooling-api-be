package eu.europa.ec.simpl.sdtoolingbe.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

class UnauthorizedExceptionTests {
	
	
	@Test
	void testConstructor() {
		UnauthorizedException ex = new UnauthorizedException("message", "code");
		assertEquals("code", ex.getCode());
		assertEquals("message", ex.getMessage());
	}
	
	@Test
	void testConstructorWithoutCode() {
		UnauthorizedException ex = new UnauthorizedException("message");
		assertNull(ex.getCode());
		assertEquals("message", ex.getMessage());
	}
	
}
