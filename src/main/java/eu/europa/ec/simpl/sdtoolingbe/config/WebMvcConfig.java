package eu.europa.ec.simpl.sdtoolingbe.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import eu.europa.ec.simpl.sdtoolingbe.constant.Constants;
import lombok.extern.log4j.Log4j2;


@Configuration
@Log4j2
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${web.mvc.cors.allowed-origins}")
    private List<String> allowedOrigins;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        if (allowedOrigins != null && !allowedOrigins.isEmpty()) {
            log.info("addCorsMappings(): setting allowed origins {}", allowedOrigins);
            registry.addMapping("/**")
                    .allowedOrigins(allowedOrigins.toArray(new String[allowedOrigins.size()]))
                    .allowedMethods("*")
                    .allowedHeaders("*")
                    .maxAge(Constants.CORS_PRE_FLIGHT_MAX_AGE)
            ;
        }
    }

}
