package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OdrlPermission {

    private String target;
    private OdrlAssignee assignee;

    @JsonProperty("action")
    private List<String> actions = new ArrayList<>();

    @JsonProperty("constraint")
    private List<OdrlConstraint> constraints = new ArrayList<>();

    public OdrlPermission(String target) {
        this.target = target;
    }

    public OdrlPermission addAction(String action) {
        actions.add(action);
        return this;
    }

    public OdrlPermission add(OdrlConstraint constraint) {
        constraints.add(constraint);
        return this;
    }

}
