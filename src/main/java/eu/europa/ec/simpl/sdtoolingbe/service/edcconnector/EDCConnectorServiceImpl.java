package eu.europa.ec.simpl.sdtoolingbe.service.edcconnector;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.OfferType;
import eu.europa.ec.simpl.sdtoolingbe.exception.EDCRegistrationException;
import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.AssetDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.ContractDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.DataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.PolicyDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.offer.infrastructure.InfrastructureDataAddress;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPermission;
import eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl.OdrlPolicy;
import eu.europa.ec.simpl.sdtoolingbe.util.json.JsonPathUtil;
import feign.FeignException;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EDCConnectorServiceImpl implements EDCConnectorService {

    private static final String RESPONSE_ID_PATH = "$.@id";

    @Value("${edc-connector.tier2-base-url}")
    private String edcConnectorTier2BaseUrl;

    private final EDCConnectorClient edcConnectorClient;
    private final Validator validator;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public EDCConnectorServiceImpl(EDCConnectorClient edcConnectorClient, Validator validator) {
        this.edcConnectorClient = edcConnectorClient;
        this.validator = validator;
    }

    @Override
    public String enrich(String sdJsonLd, String ecosystem) throws InvalidSDJsonException {
        JSONObject sdJsonLdObj = createJSONObject(sdJsonLd);

        String ns;
        if (StringUtils.isBlank(ecosystem)) {
            ns = "";
        } else {
            ns = ecosystem + ":";
        }

        JSONObject edcConnectorObj = new JSONObject();
        sdJsonLdObj.put(ns + "edcConnector", edcConnectorObj);
        edcConnectorObj.put(ns + "providerEndpointURL", edcConnectorTier2BaseUrl);
        return sdJsonLdObj.toString();
    }

    @Override
    public String register(String sdJsonLd, String ecosystem, String shapeFileName)
        throws InvalidSDJsonException, EDCRegistrationException, JsonProcessingException {
        JSONObject sdJsonLdObj = createJSONObject(sdJsonLd);

        String ns;
        if (StringUtils.isBlank(ecosystem)) {
            ns = "";
        } else {
            ns = ecosystem + ":";
        }

        OfferType offerType = toOfferType(shapeFileName);

        OdrlPolicy accessPolicy = getOdrlPolicy(sdJsonLd, replaceNS(ACCESS_POLICY_PATH, ns));
        OdrlPolicy usagePolicy = getOdrlPolicy(sdJsonLd, replaceNS(USAGE_POLICY_PATH, ns));

        String assetRegistrationId = registerAssetDefinition(offerType, ns, sdJsonLd);
        setTarget(accessPolicy, assetRegistrationId);
        setTarget(usagePolicy, assetRegistrationId);

        String accessPolicyRegistrationId = registerPolicyDefinition("accessPolicy", accessPolicy);
        String usagePolicyRegistrationId = registerPolicyDefinition("usagePolicy", usagePolicy);
        String registerContractDefinitionId = registerContractDefinition(
            assetRegistrationId, accessPolicyRegistrationId, usagePolicyRegistrationId
        );

        Map<String, String> map = Map.of(
            ns + "assetId", assetRegistrationId,
            ns + "accessPolicyId", accessPolicyRegistrationId,
            ns + "servicePolicyId", usagePolicyRegistrationId,
            ns + "contractDefinitionId", registerContractDefinitionId
        );
        sdJsonLdObj.put(ns + "edcRegistration", map);

        removeProviderDataAddress(offerType, ns, sdJsonLdObj);

        JSONObject servicePolicyObj = sdJsonLdObj.getJSONObject(ns + "servicePolicy");
        servicePolicyObj.put(ns + "access-policy", objectMapper.writeValueAsString(accessPolicy));
        servicePolicyObj.put(ns + "usage-policy", objectMapper.writeValueAsString(usagePolicy));

        return sdJsonLdObj.toString();
    }

    private static void removeProviderDataAddress(OfferType offerType, String ns, JSONObject sdJsonLdObj) {
        String propertiesName;
        switch (offerType) {
        case APPLICATION:
            propertiesName = "applicationProperties";
            break;
        case DATA:
            propertiesName = "dataProperties";
            break;
        default:
            return;
        }
        JSONObject propertiesObj = sdJsonLdObj.getJSONObject(ns + propertiesName);
        propertiesObj.remove(ns + "providerDataAddress");
        sdJsonLdObj.put(ns + propertiesName, propertiesObj);
    }

    private static OfferType toOfferType(String shapeFileName) {
        String shapeFileNameLowered = shapeFileName.toLowerCase(Locale.getDefault());
        if (shapeFileNameLowered.startsWith("application")) {
            return OfferType.APPLICATION;
        }
        if (shapeFileNameLowered.startsWith("infrastructure")) {
            return OfferType.INFRASTRUCTURE;
        }
        return OfferType.DATA;
    }

    private static JSONObject createJSONObject(String sdJsonLd) throws InvalidSDJsonException {
        try {
            return new JSONObject(sdJsonLd);
        } catch (JSONException e) {
            throw new InvalidSDJsonException(e);
        }
    }

    protected void putOptProperty(JSONObject jsonObj, String ns, String key, Map<String, String> properties) {
        String value = jsonObj.getString(ns + key);
        if (StringUtils.isNotBlank(value)) {
            properties.put(key, value);
        }
    }

    private String registerAssetDefinition(OfferType offerType, String ns, String sdJsonLd)
        throws InvalidSDJsonException, EDCRegistrationException {

        AssetDefinition assetDefinition;
        switch (offerType) {
        case APPLICATION:
            assetDefinition = createDataAssetDefinition("applicationProperties", ns, sdJsonLd);
            break;
        case DATA:
            assetDefinition = createDataAssetDefinition("dataProperties", ns, sdJsonLd);
            break;
        case INFRASTRUCTURE:
            assetDefinition = createInfrastructureAssetDefinition(ns, sdJsonLd);
            break;
        default:
            return "";
        }

        String type = "asset";
        try {
            ResponseEntity<String> response = edcConnectorClient.register(assetDefinition);
            String responseBody = getRegistrationResponseBody(type, response);
            log.debug("registerAsset(): received response body {}", responseBody);
            return JsonPath.read(responseBody, RESPONSE_ID_PATH);
        } catch (EDCRegistrationException e) {
            throw e;
        } catch (FeignException e) {
            throw createEDCRegistrationException(type, e.status(), e.contentUTF8(), e);
        } catch (Exception e) {
            throw createEDCRegistrationException(type, e);
        }
    }

    private static EDCRegistrationException createEDCRegistrationException(String type, int responseCode, String responseBody, Throwable cause) {
        return new EDCRegistrationException(
            type + " registration failed: received http response code " + responseCode + ": " + responseBody, cause
        );
    }
    
    private static EDCRegistrationException createEDCRegistrationException(String type, Throwable cause) {
        return new EDCRegistrationException(type + " registration failed", cause);
    }

    private String registerPolicyDefinition(String type, OdrlPolicy odrlPolicy) throws EDCRegistrationException {
        odrlPolicy.setType("Set");
        PolicyDefinition policyDefinition = new PolicyDefinition(odrlPolicy);
        try {
            ResponseEntity<String> response = edcConnectorClient.register(policyDefinition);
            String responseBody = getRegistrationResponseBody(type, response);
            log.debug("registerPolicyDefinition(): received response body {}", responseBody);
            return JsonPath.read(responseBody, RESPONSE_ID_PATH);
        } catch (EDCRegistrationException e) {
            throw e;
        } catch (FeignException e) {
            throw createEDCRegistrationException(type, e.status(), e.contentUTF8(), e);
        } catch (Exception e) {
            throw createEDCRegistrationException(type, e);
        }
    }

    private String registerContractDefinition(
        String assetRegistrationId, String accessPolicyRegistrationId, String usagePolicyRegistrationId
    ) throws EDCRegistrationException {
        String type = "contractDefinition";
        ContractDefinition contractDefinition = new ContractDefinition(
            assetRegistrationId, accessPolicyRegistrationId, usagePolicyRegistrationId
        );
        try {
            ResponseEntity<String> response = edcConnectorClient.register(contractDefinition);
            String responseBody = getRegistrationResponseBody(type, response);
            log.debug("registerContractDefinition(): received response body {}", responseBody);
            return JsonPath.read(responseBody, RESPONSE_ID_PATH);
        } catch (EDCRegistrationException e) {
            throw e;
        } catch (FeignException e) {
            throw createEDCRegistrationException(type, e.status(), e.contentUTF8(), e);
        } catch (Exception e) {
            throw createEDCRegistrationException(type, e);
        }
    }

    private AssetDefinition createDataAssetDefinition(String propertiesName, String ns, String sdJsonLd)
        throws InvalidSDJsonException {
        // get asset dataAddress from SD json as json string
        DataAddress dataAddress = getDataAddress(propertiesName, ns, sdJsonLd);
        AssetDefinition assetDefinition = new AssetDefinition();
        assetDefinition.setDataAddress(dataAddress);
        return assetDefinition;
    }

    private DataAddress getDataAddress(String propertiesName, String ns, String sdJsonLd)
        throws InvalidSDJsonException {
        String message = "no valid asset dataAddress json provided: ";
        try {
            String dataAddressName = ASSET_DATA_ADRESS_JSON_PATH.replace("{propertiesName}", propertiesName);
            String dataAddressAsString = JsonPathUtil.getStringValue(sdJsonLd, replaceNS(dataAddressName, ns), true);
            DataAddress result = objectMapper.readValue(dataAddressAsString, DataAddress.class);

            Set<ConstraintViolation<DataAddress>> violations = validator.validate(result);
            if (!violations.isEmpty()) {
                Optional<String> firstErrorOpt = violations.stream()
                    .findFirst()
                    .map(
                        item -> item.getPropertyPath() + ": " + item.getMessage()
                    );

                String firstError = "";
                if (firstErrorOpt.isPresent()) {
                    firstError = firstErrorOpt.get();
                }
                throw new InvalidSDJsonException(message + firstError);
            }

            return result;

        } catch (JsonProcessingException e) {
            throw new InvalidSDJsonException(message + e.getMessage(), e);
        }
    }

    private static AssetDefinition createInfrastructureAssetDefinition(String ns, String sdJsonLd)
        throws InvalidSDJsonException {
        AssetDefinition assetDefinition = new AssetDefinition();

        String provisioningAPI = JsonPathUtil
            .getStringValue(sdJsonLd, replaceNS(INFRA_PROVISIONING_API_PATH, ns), true);
        String deploymentScriptId = JsonPathUtil
            .getStringValue(sdJsonLd, replaceNS(INFRA_DEPLOYMENT_SCRIPT_ID_PATH, ns), true);
        InfrastructureDataAddress dataAddress = new InfrastructureDataAddress(provisioningAPI, deploymentScriptId);

        assetDefinition.setDataAddress(dataAddress);
        return assetDefinition;
    }

    private static String getRegistrationResponseBody(String type, ResponseEntity<String> response)
        throws EDCRegistrationException {
        int responseCode = response.getStatusCode().value();
        if (responseCode != HttpStatus.OK.value()) {
            createEDCRegistrationException(type, responseCode, response.getBody(), null);
        }
        return response.getBody();
    }

    protected String getAssetUrl(String ns, String sdJsonLd) throws InvalidSDJsonException {
        return JsonPathUtil.getStringValue(sdJsonLd, replaceNS(ASSET_URL_PATH, ns), true);
    }

    private static String replaceNS(String assetUrlPath, String ns) {
        return assetUrlPath.replace("{NS}", ns);
    }

    private OdrlPolicy getOdrlPolicy(String sdJsonLd, String path) throws InvalidSDJsonException {
        try {
            String policyJson = JsonPathUtil.getStringValue(sdJsonLd, path, true);
            return objectMapper.readValue(policyJson, OdrlPolicy.class);
        } catch (JsonProcessingException e) {
            throw new InvalidSDJsonException(e);
        }
    }

    private static void setTarget(OdrlPolicy policy, String value) {
        policy.setTarget(value);
        List<OdrlPermission> permissions = policy.getPermissions();
        if (permissions != null) {
            permissions.forEach(e -> e.setTarget(value));
        }
    }

}
