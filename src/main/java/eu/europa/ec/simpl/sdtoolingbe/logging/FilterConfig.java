package eu.europa.ec.simpl.sdtoolingbe.logging;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    FilterRegistrationBean<RequestSizeFilter> loggingFilter(){
        FilterRegistrationBean<RequestSizeFilter> registrationBean = new FilterRegistrationBean<>();

        registrationBean.setFilter(new RequestSizeFilter());
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(1);

        return registrationBean;
    }
}

