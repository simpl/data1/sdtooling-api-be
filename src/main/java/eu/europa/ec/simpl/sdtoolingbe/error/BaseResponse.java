package eu.europa.ec.simpl.sdtoolingbe.error;

import java.io.Serializable;

import lombok.ToString;

@ToString
public class BaseResponse<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String keyErrorMessage;

    private transient T response;

    public String getKeyErrorMessage() {
        return keyErrorMessage;
    }

    public void setKeyErrorMessage(String keyErrorMessage) {
        this.keyErrorMessage = keyErrorMessage;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

}
