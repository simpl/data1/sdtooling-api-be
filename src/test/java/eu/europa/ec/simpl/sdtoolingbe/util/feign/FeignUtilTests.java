package eu.europa.ec.simpl.sdtoolingbe.util.feign;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.InputStream;
import java.security.KeyStore;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.client.federatedcatalog.FederatedCatalogueTier2Client;

class FeignUtilTests {

	private static final String KEYSTORE_PWD = "keystore-generated-password";

	private SslInfo sslInfo;
	private String bearerToken = "sample-token";
	private String authorityTLSUri = "https://authority.tls.uri";
	private String targetUrl = "https://target.url";

	@BeforeEach
	void setUp() throws Exception {
		sslInfo = new SslInfo(createKeystore()).setKeyStorePassword(KEYSTORE_PWD);
	}

	@Test
	void testGetClient() {
		// Call the method we are testing
		FederatedCatalogueTier2Client client = FeignUtil.getClient(sslInfo, bearerToken, authorityTLSUri, targetUrl, null);
		assertNotNull(client);
	}

	private KeyStore createKeystore() throws Exception {
		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		try(InputStream keyStoreStream = TestSupport.getResourceAsStream("test/data-provider-eng-p12")) {
			keyStore.load(keyStoreStream, KEYSTORE_PWD.toCharArray());
			return keyStore;
		}
	}
}
