package eu.europa.ec.simpl.sdtoolingbe.properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;


class ValidationPropertiesTests {
	
	@Test
	void testModel() {
		ValidationProperties properties = new ValidationProperties();
		properties.setDomain("domain");
		properties.setValidationapi("validationapi");
		
		properties.setEnabled(true);
		assertTrue(properties.isEnabled());
		properties.setEnabled(false);
		assertFalse(properties.isEnabled());
		
		assertEquals("domain", properties.getDomain());
		assertEquals("validationapi", properties.getValidationapi());
		
		// deve ritornare domain + validationapi
		assertEquals("domainvalidationapi", properties.getValidationUrl());
	}
	
}