package eu.europa.ec.simpl.sdtoolingbe.service.credential;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.security.KeyStore;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider.AuthenticationProviderClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import feign.FeignException;

class CredentialServiceTests {

    @Mock
    private AuthenticationProviderClient authenticationProviderClient;

    @Mock
    private UsersRolesClient usersRolesClient;

    @InjectMocks
    private CredentialServiceImpl credentialService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetCredentialWithSuccess() throws Exception {
    	String pemContent = TestSupport.getResourceAsString("test/data-provider-eng-pcert", null);
    	
        String bearerToken = "testBearerToken";
        String keypairJson = 
        	"""
			{
			    "publicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEA3UlQoidirebLSc8nlOc67t2lwtI1F4AgY+4UhCzVDGy0Xt7K9C+OIbhSW4MGf9vw/8WVYIMaJS61ysGnYPSRQ==",
			    "privateKey": "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgq3DCWu8A+qRrPYDDV8LTUFqG+BF7nu4Y1EBrNXDwOfGgCgYIKoZIzj0DAQehRANCAAQDdSVCiJ2Kt5stJzyeU5zru3aXC0jUXgCBj7hSELNUMbLRe3sr0L44huFJbgwZ/2/D/xZVggxolLrXKwadg9JF"
			}
        	"""
        ;

        when(authenticationProviderClient.getKeypair(any(String.class))).thenReturn(keypairJson);
        when(usersRolesClient.getCredential(AuthBearerUtil.toBearerString(bearerToken))).thenReturn(pemContent);
        
        KeyStore result = credentialService.getCredential(bearerToken);

        assertNotNull(result);
    }

    @Test
    void testGetCredentialWithUnauthorizedException() {
        String bearerToken = "testBearerToken";

        when(authenticationProviderClient.getKeypair(any(String.class))).thenThrow(FeignException.Unauthorized.class);

        assertThrows(UnauthorizedException.class, () -> credentialService.getCredential(bearerToken));
    }

    @Test
    void testGetCredentialWithKeyStoreBuildException() {
        String bearerToken = "testBearerToken";
        String keypairJson = "invalid json";

        when(authenticationProviderClient.getKeypair(any(String.class))).thenReturn(keypairJson);

        assertThrows(KeyStoreBuildException.class, () -> credentialService.getCredential(bearerToken));
    }
}


