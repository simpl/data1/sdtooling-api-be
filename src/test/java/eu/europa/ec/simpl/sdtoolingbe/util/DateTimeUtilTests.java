package eu.europa.ec.simpl.sdtoolingbe.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.junit.jupiter.api.Test;

class DateTimeUtilTests {

	@Test
    void testDateTimeToStringWithZoneId() {
        // Data di esempio
        Date date = Date.from(Instant.parse("2023-09-24T10:15:30Z"));
        ZoneId zoneId = ZoneId.of("Europe/Paris");

        // Conversione a stringa con il fuso orario specificato
        String result = DateTimeUtil.dateTimeToString(date, zoneId);

        // Verifica del risultato
        assertEquals("2023-09-24T12:15:30+02:00", result);
    }

    @Test
    void testDateTimeToStringWithNullZoneIdAndDefaultGMT() {
        // Data di esempio
        Date date = Date.from(Instant.parse("2023-09-24T10:15:30Z"));

        // Conversione a stringa con zoneId null (deve usare GMT)
        String result = DateTimeUtil.dateTimeToString(date, null);

        // Verifica del risultato
        assertEquals("2023-09-24T10:15:30Z", result);
    }

    @Test
    void testDateTimeFromStringWithTimezone() {
        // Stringa con timezone
        String dateTimeAsString = "2023-09-24T10:15:30+02:00";

        // Conversione da stringa a Date
        Date result = DateTimeUtil.dateTimeFromString(dateTimeAsString);

        // Verifica del risultato
        assertEquals(Date.from(Instant.parse("2023-09-24T08:15:30Z")), result);
    }

    @Test
    void testDateTimeFromStringWithoutTimezone() {
        // Stringa senza timezone (deve assumere GMT)
        String dateTimeAsString = "2023-09-24T10:15:30";

        // Conversione da stringa a Date
        Date result = DateTimeUtil.dateTimeFromString(dateTimeAsString);

        // Verifica del risultato
        assertEquals(Date.from(Instant.parse("2023-09-24T10:15:30Z")), result);
    }

    @Test
    void testDateTimeFromStringInvalidFormat() {
        // Stringa non valida
        String invalidDateTimeAsString = "invalid-date-time-format";

        // Verifica che venga lanciata un'eccezione DateTimeParseException
        assertThrows(DateTimeParseException.class, () -> {
            DateTimeUtil.dateTimeFromString(invalidDateTimeAsString);
        });
    }
	
	
}
