package eu.europa.ec.simpl.sdtoolingbe.service.conversionservice;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;

@ExtendWith(MockitoExtension.class)
class ConversionServiceTests {
    
    private Map<String, Map<String, List<String>>> ecosystemToCategoryToShaclFileMap;
	
	@InjectMocks
    private ConversionServiceImpl conversionService;

    @Mock
    private FileUtils fileUtils;

    @Mock
    private ObjectMapper objectMapper;
    
    @BeforeEach
    void setUp() {
        ecosystemToCategoryToShaclFileMap = new HashMap<>();
        Map<String, List<String>> categoryToFiles = new HashMap<>();
        categoryToFiles.put("category1", List.of("file1.ttl", "file2.ttl"));
        ecosystemToCategoryToShaclFileMap.put("ecosystem1", categoryToFiles);
    }
    
    @Test
    void testGetTTL_ecosystemNotFound() {
        // Arrange
        String ecosystem = "nonexistent";
        String name = "data-offeringShape";
        ReflectionTestUtils.setField(conversionService, "ecosystemToCategoryToShaclFileMap", ecosystemToCategoryToShaclFileMap);

        assertThrows(BadRequestException.class,
                () -> conversionService.getTTL(ecosystem, name)
        );
    }
    
    @Test
    void testGetTTLFileNotFound() {
        // Arrange
        String ecosystem = "simpl";
        String name = "nonexistent.json";
        ReflectionTestUtils.setField(conversionService, "ecosystemToCategoryToShaclFileMap", ecosystemToCategoryToShaclFileMap);

        assertThrows(BadRequestException.class,
                () -> conversionService.getTTL(ecosystem, name)
        );
    }
    
    /* cannot be tested
    @Test
    void testGetTTLTraversalPath() {
        // Arrange
        String ecosystem = "../simpl";
        String name = "data-offeringShape";

        assertThrows(IllegalArgumentException.class,
                () -> conversionService.getTTL(ecosystem, name)
        );
    }
    */

}
