package eu.europa.ec.simpl.sdtoolingbe.service.jwt;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;

class JWTServiceTests {
	
	private JWTService jwtService = new JWTServiceImpl();

    @Test
    void testCheckJWTWithValidToken() {
        String jwtToken = TestSupport.createValidJwt();
        // Verifica che non venga lanciata nessuna eccezione con token valido
        assertDoesNotThrow(() -> jwtService.checkJWT(jwtToken));
    }

    @Test
    void testCheckJWTWithInvalidToken() {
    	String jwtToken = "invalid json as JWT token";
        // Verifica che venga lanciata l'eccezione UnauthorizedException con token non valido
        assertThrows(UnauthorizedException.class, () -> jwtService.checkJWT(jwtToken));
    }
	
}
