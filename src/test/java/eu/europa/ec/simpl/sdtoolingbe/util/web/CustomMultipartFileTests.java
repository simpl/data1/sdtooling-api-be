package eu.europa.ec.simpl.sdtoolingbe.util.web;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class CustomMultipartFileTests {
	
	private CustomMultipartFile customMultipartFile;
    private String name = "test-file.txt";
    private String originalFilename = "original-test-file.txt";
    private String contentType = "text/plain";
    private byte[] content = "Sample content for testing".getBytes();

    @BeforeEach
    void setUp() {
        customMultipartFile = new CustomMultipartFile(name, originalFilename, contentType, content);
    }

    @Test
    void testGetName() {
        assertEquals(name, customMultipartFile.getName());
    }

    @Test
    void testGetOriginalFilename() {
        assertEquals(originalFilename, customMultipartFile.getOriginalFilename());
    }

    @Test
    void testGetContentType() {
        assertEquals(contentType, customMultipartFile.getContentType());
    }

    @Test
    void testIsEmptyWhenContentIsNotEmpty() {
        assertFalse(customMultipartFile.isEmpty());
    }

    @Test
    void testIsEmptyWhenContentIsEmpty() {
        CustomMultipartFile emptyFile = new CustomMultipartFile(name, originalFilename, contentType, new byte[0]);
        assertTrue(emptyFile.isEmpty());
    }
    
    @Test
    void testIsEmptyWhenContentIsNull() {
        CustomMultipartFile emptyFile = new CustomMultipartFile(name, originalFilename, contentType, null);
        assertTrue(emptyFile.isEmpty());
    }

    @Test
    void testGetSize() {
        assertEquals(content.length, customMultipartFile.getSize());
    }

    @Test
    void testGetBytes() throws IOException {
        assertArrayEquals(content, customMultipartFile.getBytes());
    }

    @Test
    void testGetInputStream() throws IOException {
		try (InputStream inputStream = customMultipartFile.getInputStream()) {
			assertNotNull(inputStream);
			assertTrue(inputStream instanceof ByteArrayInputStream);

			byte[] readContent = new byte[content.length];
			assertEquals(content.length, inputStream.read(readContent));
			assertArrayEquals(content, readContent);
		}
    }

    @Test
    void testTransferToNotSupported() {
        File dest = new File("test-destination.txt");
        UnsupportedOperationException exception = assertThrows(UnsupportedOperationException.class, () -> {
            customMultipartFile.transferTo(dest);
        });
        assertEquals("transferTo not supported", exception.getMessage());
    }
}
