package eu.europa.ec.simpl.sdtoolingbe.exception;

public class EDCRegistrationException extends Exception {

    public EDCRegistrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public EDCRegistrationException(String message) {
        super(message);
    }

    public EDCRegistrationException(Throwable cause) {
        super(cause);
    }

}
