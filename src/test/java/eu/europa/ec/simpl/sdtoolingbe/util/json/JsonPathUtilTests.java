package eu.europa.ec.simpl.sdtoolingbe.util.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;

class JsonPathUtilTests {

    @Test
    void testGetStringValue() throws InvalidSDJsonException {
        String json = "{ \"key\": \"value\" }";
        String path = "$.key";
        boolean mandatory = true;

        String result = JsonPathUtil.getStringValue(json, path, mandatory);

        assertEquals("value", result);
    }

    @Test
    void testGetStringValueWithMandatoryAndValueBlank() {
        String json = "{ \"key\": \" \" }"; // valore vuoto
        String path = "$.key";
        boolean mandatory = true;

        InvalidSDJsonException exception = assertThrows(
            InvalidSDJsonException.class,
            () -> JsonPathUtil.getStringValue(json, path, mandatory)
        );

        assertEquals("no value found for path '" + path + "'", exception.getMessage());
    }

    @Test
    void testGetStringValueWithPathNotFound() {
        String json = "{ \"key\": \"value\" }";
        String path = "$.nonexistentKey"; // percorso non esistente
        boolean mandatory = true;

        InvalidSDJsonException exception = assertThrows(
            InvalidSDJsonException.class,
            () -> JsonPathUtil.getStringValue(json, path, mandatory)
        );

        assertEquals("no element found for path '" + path + "'", exception.getMessage());
    }

    @Test
    void testGetStringValueNonMandatoryAndValueBlank() throws InvalidSDJsonException {
        String json = "{ \"key\": \" \" }"; // valore vuoto
        String path = "$.key";
        boolean mandatory = false;

        String result = JsonPathUtil.getStringValue(json, path, mandatory);

        assertTrue(StringUtils.isBlank(result));
    }

    @Test
    void testGetStringValueNonMandatoryAndPathNotFound() {
        String json = "{ \"key\": \"value\" }";
        String path = "$.nonexistentKey"; // percorso non esistente
        boolean mandatory = false;

        InvalidSDJsonException exception = assertThrows(
            InvalidSDJsonException.class,
            () -> JsonPathUtil.getStringValue(json, path, mandatory)
        );

        assertEquals("no element found for path '" + path + "'", exception.getMessage());
    }
}

