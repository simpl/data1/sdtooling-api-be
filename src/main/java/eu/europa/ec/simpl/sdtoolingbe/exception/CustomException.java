package eu.europa.ec.simpl.sdtoolingbe.exception;

public class CustomException extends RuntimeException {

    private final String code;
    private final String errorTitle;
    
    /**
     * @param code
     * @param errorTitle use the title also as exception message
     */
    public CustomException(String code, String errorTitle) {
        super(errorTitle);
        this.code = code;
        this.errorTitle = errorTitle;
    }

    public CustomException(String code, String errorTitle, String errorMessage) {
        super(errorMessage);
        this.code = code;
        this.errorTitle = errorTitle;
    }
    
    /**
     * 
     * @param code
     * @param errorTitle use the title also as exception message
     * @param cause
     */
    public CustomException(String code, String errorTitle, Throwable cause) {
        super(errorTitle, cause);
        this.code = code;
        this.errorTitle = errorTitle;
    }
    
    public CustomException(String code, String errorTitle, String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.code = code;
        this.errorTitle = errorTitle;
    }

    public String getCode() {
        return code;
    }

    public String getErrorTitle() {
        return errorTitle;
    }
}
