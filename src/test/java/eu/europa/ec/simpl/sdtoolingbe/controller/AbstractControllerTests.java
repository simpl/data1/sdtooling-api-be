package eu.europa.ec.simpl.sdtoolingbe.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.error.BaseResponse;
import eu.europa.ec.simpl.sdtoolingbe.error.ErrorResponse;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.util.json.JsonDeserializerException;
import jakarta.servlet.http.HttpServletRequest;

class AbstractControllerTests extends AbstractController {
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private MissingServletRequestPartException missingServletRequestPartException;
	@Mock
	private MethodArgumentNotValidException methodArgumentNotValidException;
	@Mock
    private HttpMessageNotReadableException httpMessageNotReadableException;
	@Mock
    private CustomException customException;
	
	@Mock
    private ObjectMapper mockObjectMapper;
	
	@Mock
	private BindingResult bindingResult;
	
	@BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }
	
	@Test
	void testMissingServletRequestPartException() {
		BaseResponse<ErrorResponse> response = missingServletRequestPartException(request, missingServletRequestPartException);
		
		assertNotNull(response);
		assertNotNull(response.getResponse());
		assertEquals("Missing argument", response.getResponse().getErrorTitle());
		assertEquals(ErrorCode.VALIDATION_ERROR.getKeyErrorMessage(), response.getKeyErrorMessage());
	}
	
	@Test
	void testMethodArgumentNotValidException() {
		String expectedErrorMessage = "This is an error message";
		FieldError fieldError = new FieldError("objectName", "field", expectedErrorMessage);
		
		when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);
		when(bindingResult.getFieldErrors()).thenReturn(Collections.singletonList(fieldError));
		
		BaseResponse<ErrorResponse> response = methodArgumentNotValidException(request, methodArgumentNotValidException);
		
		assertNotNull(response);
		assertNotNull(response.getResponse());
		assertEquals("Invalid payload", response.getResponse().getErrorTitle());
		assertEquals(expectedErrorMessage, response.getResponse().getErrorDescription());
		assertEquals(ErrorCode.VALIDATION_ERROR.getKeyErrorMessage(), response.getKeyErrorMessage());

	}
	
	@Test
    void testMethodArgumentNotValidExceptionWithJsonDeserializerException() {
        String expectedErrorMessage = "This is a deserialization error message";
        JsonDeserializerException jsonDeserializerException = new JsonDeserializerException(expectedErrorMessage);

        when(httpMessageNotReadableException.getCause()).thenReturn(jsonDeserializerException);
        when(httpMessageNotReadableException.getMessage()).thenReturn("Default error message");

        BaseResponse<ErrorResponse> response = methodArgumentNotValidException(request, httpMessageNotReadableException);

        assertNotNull(response);
        assertNotNull(response.getResponse());
        assertEquals("Invalid payload", response.getResponse().getErrorTitle());
        assertEquals(expectedErrorMessage, response.getResponse().getErrorDescription());
        assertEquals(ErrorCode.VALIDATION_ERROR.getKeyErrorMessage(), response.getKeyErrorMessage());
    }

    @Test
    void testMethodArgumentNotValidExceptionWithoutJsonDeserializerException() {
        String expectedErrorMessage = "Default error message";
        
        when(httpMessageNotReadableException.getCause()).thenReturn(null);
        when(httpMessageNotReadableException.getMessage()).thenReturn(expectedErrorMessage);

        BaseResponse<ErrorResponse> response = methodArgumentNotValidException(request, httpMessageNotReadableException);

        assertNotNull(response);
        assertNotNull(response.getResponse());
        assertEquals("Invalid payload", response.getResponse().getErrorTitle());
        assertEquals(expectedErrorMessage, response.getResponse().getErrorDescription());
        assertEquals(ErrorCode.VALIDATION_ERROR.getKeyErrorMessage(), response.getKeyErrorMessage());
    }
    
    
    @Test
    void testHashGenerationExceptionSuccess() {
        String errorMessage = "Custom error message";
        String errorTitle = "Custom error title";
        String errorCode = "123";
        String requestUrl = "http://localhost/request";
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage(errorCode);
        expectedResponse.setResponse(new ErrorResponse(errorTitle, errorMessage));

        when(customException.getCode()).thenReturn(errorCode);
        when(customException.getErrorTitle()).thenReturn(errorTitle);
        when(customException.getMessage()).thenReturn(errorMessage);
        when(request.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        
        BaseResponse<ErrorResponse> response = hashGenerationException(request, customException);

        assertNotNull(response);
        assertEquals(errorCode, response.getKeyErrorMessage());
        assertEquals(errorTitle, response.getResponse().getErrorTitle());
        assertEquals(errorMessage, response.getResponse().getErrorDescription());
    }

    @SuppressWarnings("serial")
	@Test
    void testHashGenerationExceptionJsonProcessingException() throws Exception {
    	
    	super.objectMapper = mockObjectMapper;
    	
        String errorMessage = "Custom error message";
        String errorTitle = "Custom error title";
        String errorCode = "123";
        String requestUrl = "http://localhost/request";
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage(errorCode);
        expectedResponse.setResponse(new ErrorResponse(errorTitle, errorMessage));

        when(customException.getCode()).thenReturn(errorCode);
        when(customException.getErrorTitle()).thenReturn(errorTitle);
        when(customException.getMessage()).thenReturn(errorMessage);
        when(request.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
        
        // simulo la JsonProcessingException invocando il mock objectMapper
        when(mockObjectMapper.writeValueAsString(any())).thenThrow(new JsonProcessingException("Test exception") {});
       
        BaseResponse<ErrorResponse> response = hashGenerationException(request, customException);

        assertNotNull(response);
        assertEquals(errorCode, response.getKeyErrorMessage());
        assertEquals(errorTitle, response.getResponse().getErrorTitle());
        assertEquals(errorMessage, response.getResponse().getErrorDescription());
    }
    
    
    @Test
    void testBadRequestExceptionSuccess() {
        BadRequestException badRequestException = new BadRequestException("ERROR_CODE", "Error Message");
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage("ERROR_CODE");
        expectedResponse.setResponse(new ErrorResponse("Error Message"));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost/test"));
        
        BaseResponse<ErrorResponse> actualResponse = badRequestException(request, badRequestException);

        assertEquals(expectedResponse.getKeyErrorMessage(), actualResponse.getKeyErrorMessage());
        assertEquals(expectedResponse.getResponse().getErrorDescription(), actualResponse.getResponse().getErrorDescription());
    }

    @SuppressWarnings("serial")
	@Test
    void testBadRequestExceptionJsonProcessingException() throws JsonProcessingException {
    	super.objectMapper = mockObjectMapper;
    	
        BadRequestException badRequestException = new BadRequestException("ERROR_CODE", "Error Message");
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage("ERROR_CODE");
        expectedResponse.setResponse(new ErrorResponse("Error Message"));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost/test"));
        when(mockObjectMapper.writeValueAsString(any())).thenThrow(new JsonProcessingException("Test exception") {});

        BaseResponse<ErrorResponse> actualResponse = badRequestException(request, badRequestException);

        assertEquals(expectedResponse.getKeyErrorMessage(), actualResponse.getKeyErrorMessage());
        assertEquals(expectedResponse.getResponse().getErrorDescription(), actualResponse.getResponse().getErrorDescription());
    }
    
    
    @Test
    void testUnauthorizedExceptionSuccess() {
        UnauthorizedException unauthorizedException = new UnauthorizedException("Unauthorized access", "UNAUTHORIZED_CODE");
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage("UNAUTHORIZED_CODE");
        expectedResponse.setResponse(new ErrorResponse("Unauthorized access"));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost/test"));
        
        BaseResponse<ErrorResponse> actualResponse = unauthorizedException(request, unauthorizedException);

        assertEquals(expectedResponse.getKeyErrorMessage(), actualResponse.getKeyErrorMessage());
        assertEquals(expectedResponse.getResponse().getErrorDescription(), actualResponse.getResponse().getErrorDescription());
    }

    @SuppressWarnings("serial")
	@Test
    void testUnauthorizedExceptionJsonProcessingException() throws JsonProcessingException {
    	super.objectMapper = mockObjectMapper;
    	
        UnauthorizedException unauthorizedException = new UnauthorizedException("Unauthorized access", "UNAUTHORIZED_CODE");
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage("UNAUTHORIZED_CODE");
        expectedResponse.setResponse(new ErrorResponse("Unauthorized access"));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost/test"));
        when(mockObjectMapper.writeValueAsString(any())).thenThrow(new JsonProcessingException("Test exception") {});

        BaseResponse<ErrorResponse> actualResponse = unauthorizedException(request, unauthorizedException);

        assertEquals(expectedResponse.getKeyErrorMessage(), actualResponse.getKeyErrorMessage());
        assertEquals(expectedResponse.getResponse().getErrorDescription(), actualResponse.getResponse().getErrorDescription());
    }
    
    
    @Test
    void testGeneralExceptionSuccess() {
		Throwable throwable = new Throwable("General error occurred");
		BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
		expectedResponse.setKeyErrorMessage(ErrorCode.GENERAL_ERROR.toString());
		expectedResponse.setResponse(new ErrorResponse("Please try again"));
		
		when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost/test"));
		   
		BaseResponse<ErrorResponse> actualResponse = generalException(request, throwable);
		
		assertEquals(expectedResponse.getKeyErrorMessage(), actualResponse.getKeyErrorMessage());
		assertEquals(expectedResponse.getResponse().getErrorDescription(), actualResponse.getResponse().getErrorDescription());
    }

    @SuppressWarnings("serial")
	@Test
    void testGeneralExceptionJsonProcessingException() throws JsonProcessingException {
        super.objectMapper = mockObjectMapper;
    	
        Throwable throwable = new Throwable("General error occurred");
        BaseResponse<ErrorResponse> expectedResponse = new BaseResponse<>();
        expectedResponse.setKeyErrorMessage(ErrorCode.GENERAL_ERROR.toString());
        expectedResponse.setResponse(new ErrorResponse("Please try again"));

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://localhost/test"));
        when(mockObjectMapper.writeValueAsString(any(BaseResponse.class))).thenThrow(new JsonProcessingException("Error") {});

        BaseResponse<ErrorResponse> actualResponse = generalException(request, throwable);

        assertEquals(expectedResponse.getKeyErrorMessage(), actualResponse.getKeyErrorMessage());
        assertEquals(expectedResponse.getResponse().getErrorDescription(), actualResponse.getResponse().getErrorDescription());
    }
    
}
