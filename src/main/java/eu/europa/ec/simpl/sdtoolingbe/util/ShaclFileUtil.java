package eu.europa.ec.simpl.sdtoolingbe.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import org.apache.commons.io.FileUtils;

import lombok.extern.log4j.Log4j2;

@Log4j2
public final class ShaclFileUtil {

    private static final String SHAPES_DIR = Paths.get("data", "shapes").toString();

    public static final String JSON_FILE_ENDING = ".json";
    public static final String TTL_FILE_ENDING = ".ttl";

    private ShaclFileUtil() {
    }
    
    public static File getShapesDir() {
        return new File(SHAPES_DIR);
    }

    public static File getShapesDir(String ecosystem) {
        checkPathTraversal(ecosystem, "ecosystem");
        return new File(Paths.get(SHAPES_DIR, ecosystem).toString());
    }
    
    public static Path getShapesFilePath(String ecosystem, String schemaType, String filename) {
        checkPathTraversal(ecosystem, "ecosystem");
        checkPathTraversal(schemaType, "schemaType");
        checkPathTraversal(filename, "name");
        return getShapesDir().toPath().resolve(ecosystem).resolve(schemaType).resolve(filename).normalize();
    }

    public static String getTtlFromFilename(String ecosystem, String shapeFileName) throws IOException {
        String method = "Method: getTtlFromFilename - ";
        log.info("{}{}", method, "Init");

        File shapesDir = getShapesDir(ecosystem);

        for (File dir : Objects.requireNonNullElse(shapesDir.listFiles(), new File[0])) {
            if (dir.isDirectory()) {
                File shapesSubDir = new File(Paths.get(dir.toString()).toString());
                File[] files = shapesSubDir.listFiles();
                log.info("{}Receive shapeFileName: {}", method, shapeFileName);

                if (files != null) {
                    for (File file : files) {
                        if (file.getName().equals(shapeFileName)) {
                            log.info("{}File found with name: {}", method, shapeFileName);
                            return FileUtils.readFileToString(file, Charset.defaultCharset());
                        }
                    }
                    log.warn("{}File NOT found with name: {}", method, shapeFileName);
                } else {
                    log.warn("{}Directory is empty or does not exist", method);
                }
            }
        }
        return null;
    }
    
    public static void checkPathTraversal(String value, String name) {
        if(value!=null && (value.contains("..") || value.contains("/"))) {
            throw new IllegalArgumentException("invalid " + name + " value");
        }
    }

}
