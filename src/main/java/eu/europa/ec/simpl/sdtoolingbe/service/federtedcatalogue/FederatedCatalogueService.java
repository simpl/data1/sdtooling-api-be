package eu.europa.ec.simpl.sdtoolingbe.service.federtedcatalogue;

public interface FederatedCatalogueService {

    /**
     * Call the Tier2 gateway using the FederatedCatalogueTier2Client passing the
     * federated catalogue access token (to be changed in the future: catalogue
     * should use tier1 access token)
     * 
     * @param sdJsonLd
     * @param tier1AccessToken used to request the participant credential
     *                         certificate on the fly
     * @return
     * @throws Exception
     */
    String publishSD(String sdJsonLd, String tier1BearerToken) throws Exception;

}
