package eu.europa.ec.simpl.sdtoolingbe.service.edcconnector;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import com.jayway.jsonpath.JsonPath;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;
import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.OfferType;
import eu.europa.ec.simpl.sdtoolingbe.exception.EDCRegistrationException;
import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.AssetDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.ContractDefinition;
import eu.europa.ec.simpl.sdtoolingbe.model.edc.management.PolicyDefinition;
import feign.FeignException;
import feign.Request;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

class EDCConnectorServiceTests {
	
	private EDCConnectorService connectorService;
	
	private static final String ECOSYSTEM = "simpl";
	private static final String NS = StringUtils.isBlank(ECOSYSTEM)?"":ECOSYSTEM + ":"; 
	
	private static final String DATA_OFFERING_SHAPE_FILE_NAME = "data-offeringShape";
	private static final String INFRA_OFFERING_SHAPE_FILE_NAME = "infrastructure-offeringShape";
	
	@Mock
	private EDCConnectorClient edcConnectorClient;
	
	@BeforeEach
	void setUpEach() {
		MockitoAnnotations.openMocks(this);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
		connectorService = new EDCConnectorServiceImpl(edcConnectorClient, validator);
		ReflectionTestUtils.setField(connectorService, "edcConnectorTier2BaseUrl", "https://edc-connector-tier2-base-url");
	}
	
	@Test
    void testEnrich() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
        String result = connectorService.enrich(sdJsonLd, ECOSYSTEM);
        assertNotNull(result);
    	assertTrue(StringUtils.isNotBlank(JsonPath.read(result, "$." + NS + "edcConnector." + NS + "providerEndpointURL")));
    }

    @Test
    void testEnrichWithEmptyEcosystem() throws Exception {
    	String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
       
    	String result = connectorService.enrich(sdJsonLd, "");
        assertNotNull(result);
        assertTrue(StringUtils.isNotBlank(JsonPath.read(result, "$.edcConnector.providerEndpointURL")));
    }

    @Test
    void testEnrichWithInvalidJsonInput() {
    	assertThrows(InvalidSDJsonException.class, () -> connectorService.enrich("not a json string", ECOSYSTEM));
    }
    
    
    
	private static Stream<Arguments> TestRegister() {
		// offeringFileName
		return Stream.of(
			Arguments.of(OfferType.DATA, "test/frontend-NEW-data-offering.json", DATA_OFFERING_SHAPE_FILE_NAME),
			Arguments.of(OfferType.INFRASTRUCTURE, "test/frontend-NEW-infra-offering.json", INFRA_OFFERING_SHAPE_FILE_NAME)
		);
	}
	
	@ParameterizedTest
	@MethodSource("TestRegister")
	void testRegister(OfferType offerType, String offeringFile, String shapeFileName) throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString(offeringFile, null);
    	
    	String responseBody = "{\"@id\":\"f31452f6-2d41-4edd-bf1f-e06329c244d9\"}";
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(new ResponseEntity<String>(responseBody, HttpStatus.OK));
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(new ResponseEntity<String>(responseBody, HttpStatus.OK));
    	when(edcConnectorClient.register(any(ContractDefinition.class))).thenReturn(new ResponseEntity<String>(responseBody, HttpStatus.OK));
    	
    	String result = connectorService.register(sdJsonLd, ECOSYSTEM, shapeFileName);
    	
    	assertNotNull(result);
    	assertTrue(StringUtils.isNotBlank(JsonPath.read(result, "$." + NS + "edcRegistration." + NS + "assetId")));
    	assertTrue(StringUtils.isNotBlank(JsonPath.read(result, "$." + NS + "edcRegistration." + NS + "accessPolicyId")));
    	assertTrue(StringUtils.isNotBlank(JsonPath.read(result, "$." + NS + "edcRegistration." + NS + "servicePolicyId")));
    	assertTrue(StringUtils.isNotBlank(JsonPath.read(result, "$." + NS + "edcRegistration." + NS + "contractDefinitionId")));
    	
    	// verifico che sia stato rimosso generalServiceProperties.providerDataAddress
    	assertNull(new JSONObject(result).getJSONObject( NS + "generalServiceProperties").optJSONObject(NS + "providerDataAddress"));
    	
	}
	
	@Test
	void testRegisterWithEmptyEcosystem() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
		
    	String responseBody = "{\"@id\":\"f31452f6-2d41-4edd-bf1f-e06329c244d9\"}";
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(new ResponseEntity<String>(responseBody, HttpStatus.OK));
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(new ResponseEntity<String>(responseBody, HttpStatus.OK));
    	when(edcConnectorClient.register(any(ContractDefinition.class))).thenReturn(new ResponseEntity<String>(responseBody, HttpStatus.OK));
    	
    	
    	assertThrows(InvalidSDJsonException.class, () -> connectorService.register(sdJsonLd, "", DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterWithInvalidJsonLD() {
    	assertThrows(InvalidSDJsonException.class, () -> connectorService.register("not a json string", ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterWithInvalidDataAddress() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	// removing mandatory storage value from providerDataAddress
    	JSONObject jsonObj = new JSONObject(sdJsonLd);
    	JSONObject propertiesObj = jsonObj.getJSONObject(NS + "dataProperties");
    	JSONObject providerDataAddressObj = new JSONObject(propertiesObj.getString(NS + "providerDataAddress"));
    	providerDataAddressObj.put("storage", (String)null);
		propertiesObj.put(NS + "providerDataAddress", providerDataAddressObj.toString());
    	
		assertThrows(InvalidSDJsonException.class, () -> connectorService.register(jsonObj.toString(), ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterWithInvalidDataAddressJson() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	// replacing providerDataAddress value with an invalid json string format
    	JSONObject jsonObj = new JSONObject(sdJsonLd);
    	JSONObject propertiesObj = jsonObj.getJSONObject(NS + "dataProperties");
		propertiesObj.put(NS + "providerDataAddress", "not a valid json");
    	
		assertThrows(InvalidSDJsonException.class, () -> connectorService.register(jsonObj.toString(), ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterNoValueFound() {
    	assertThrows(InvalidSDJsonException.class, () -> connectorService.register("{\"param\":\"value\"}", ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterAssetWithEDCRegistrationException() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> invalidResponseEntity = new ResponseEntity<>("no json content", HttpStatus.OK);	
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(invalidResponseEntity);
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	private static Stream<Arguments> WithExceptionsParameters() {
		Request request = mock(Request.class);
		return Stream.of(
			Arguments.of(new FeignException.BadRequest("test", request, "test".getBytes(), null)),
			Arguments.of(new RuntimeException("test"))
		);
	}
	
	@ParameterizedTest
	@MethodSource("WithExceptionsParameters")
	void testRegisterAssetWithExceptions(Exception e) throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
		when(edcConnectorClient.register(any(AssetDefinition.class))).thenThrow(e);
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterAssetWithNotOKresponseCode() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> notOKResponseEntity = new ResponseEntity<>("does not care", HttpStatus.ACCEPTED);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(notOKResponseEntity);
    	
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterPolicyWithEDCRegistrationException() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> validResponseEntity = new ResponseEntity<>("{\"@id\": \"fbda41f7-d339-4b44-8919-dce1a89f8e70\"}", HttpStatus.OK);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(validResponseEntity);
    	
    	ResponseEntity<String> invalidResponseEntity = new ResponseEntity<>("no json content", HttpStatus.OK);
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(invalidResponseEntity);
    	
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@ParameterizedTest
	@MethodSource("WithExceptionsParameters")
	void testRegisterPolicyWithExceptions(Exception e) throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> validResponseEntity = new ResponseEntity<>("{\"@id\": \"fbda41f7-d339-4b44-8919-dce1a89f8e70\"}", HttpStatus.OK);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(validResponseEntity);
    	
		when(edcConnectorClient.register(any(PolicyDefinition.class))).thenThrow(e);
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	

	@Test
	void testRegisterPolicyWithNotOKresponseCode() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> validResponseEntity = new ResponseEntity<>("{\"@id\": \"fbda41f7-d339-4b44-8919-dce1a89f8e70\"}", HttpStatus.OK);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(validResponseEntity);
    	
    	ResponseEntity<String> notOKResponseEntity = new ResponseEntity<>("does not care", HttpStatus.ACCEPTED);
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(notOKResponseEntity);
    	
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterContractWithEDCRegistrationException() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> validResponseEntity = new ResponseEntity<>("{\"@id\": \"fbda41f7-d339-4b44-8919-dce1a89f8e70\"}", HttpStatus.OK);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(validResponseEntity);
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(validResponseEntity);
    	
    	ResponseEntity<String> invalidResponseEntity = new ResponseEntity<>("no json content", HttpStatus.OK);
    	when(edcConnectorClient.register(any(ContractDefinition.class))).thenReturn(invalidResponseEntity);
    	
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@ParameterizedTest
	@MethodSource("WithExceptionsParameters")
	void testRegisterContractWithExceptions(Exception e) throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> validResponseEntity = new ResponseEntity<>("{\"@id\": \"fbda41f7-d339-4b44-8919-dce1a89f8e70\"}", HttpStatus.OK);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(validResponseEntity);
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(validResponseEntity);
    	
		when(edcConnectorClient.register(any(ContractDefinition.class))).thenThrow(e);
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}
	
	@Test
	void testRegisterContractWithNotOKresponseCode() throws Exception {
		String sdJsonLd = TestSupport.getResourceAsString("test/frontend-NEW-data-offering.json", null);
    	
    	ResponseEntity<String> validResponseEntity = new ResponseEntity<>("{\"@id\": \"fbda41f7-d339-4b44-8919-dce1a89f8e70\"}", HttpStatus.OK);
    	when(edcConnectorClient.register(any(AssetDefinition.class))).thenReturn(validResponseEntity);
    	when(edcConnectorClient.register(any(PolicyDefinition.class))).thenReturn(validResponseEntity);
    	
    	ResponseEntity<String> notOKResponseEntity = new ResponseEntity<>("does not care", HttpStatus.ACCEPTED);
    	when(edcConnectorClient.register(any(ContractDefinition.class))).thenReturn(notOKResponseEntity);
    	
    	assertThrows(EDCRegistrationException.class, () -> connectorService.register(sdJsonLd, ECOSYSTEM, DATA_OFFERING_SHAPE_FILE_NAME));
	}

}