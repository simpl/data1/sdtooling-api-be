package eu.europa.ec.simpl.sdtoolingbe.util.json;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

/**
 * Jackson JSON deserializer che converte una stringa in formato ISO8601 in un oggetto Date.
 * 
 * @author apadula
 */
public class ISO8601LocalDateJsonDeserializer extends JsonDeserializerSupport<Date> {

    @Override
    public Date deserialize(JsonParser parser, DeserializationContext context)
        throws IOException {
        String dateAsString = parser.getText();
        if (StringUtils.isBlank(dateAsString)) {
            return null;
        }

        /*
         * accetta i formati senza il timezone: '2011-12-03T10:15:30'
         */
        try {
            LocalDateTime datetime = LocalDateTime.parse(dateAsString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            Instant instant = ZonedDateTime.of(datetime, TimeZone.getDefault().toZoneId()).toInstant();
            return Date.from(instant);
        } catch (DateTimeParseException ex) {
            throw newUnparsableDateTimeException(parser);
        }
    }

}
