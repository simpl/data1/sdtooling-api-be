package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.web.servlet.MockMvc;

import eu.europa.ec.simpl.sdtoolingbe.client.ValidationClient;
import eu.europa.ec.simpl.sdtoolingbe.client.authenticationprovider.AuthenticationProviderClient;
import eu.europa.ec.simpl.sdtoolingbe.client.edcconnector.EDCConnectorClient;
import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationServiceImpl;

@WebMvcTest(value = ContractController.class)
@TestPropertySource(properties = "web.mvc.bearer-token.required=false")
class ContractControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockitoBean
    private ValidationClient validationClient;
    
    @MockitoBean
    private UsersRolesClient usersRolesClient;

    @MockitoBean
    private AuthenticationProviderClient authenticationProviderClient;

    @MockitoBean
    private EDCConnectorClient edcConnectorClient;

	@MockitoBean
	private ValidationServiceImpl validationService;
	
	@MockitoBean
	private HealthEndpoint healthEndpoint;


    @Test
    void testValidate() throws Exception {
        String sdJsonLd = "{\"key\": \"value\"}";
        String ecosystem = "simpl";
        String shapeFileName = "any";

        MockMultipartFile jsonFile = new MockMultipartFile(
                "json-file",
                "test.json",
                MediaType.APPLICATION_JSON_VALUE,
                sdJsonLd.getBytes(StandardCharsets.UTF_8)
        );

        doNothing().when(validationService).validateJsonLd(anyString(), anyString(), anyString());

        mockMvc.perform(multipart("/api/contract/v1/validate")
                        .file(jsonFile)
                        .param("ecosystem", ecosystem)
                        .param("shapeFileName", shapeFileName)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                )
                .andExpect(status().isOk())
        ;

    }

}
