package eu.europa.ec.simpl.sdtoolingbe.controller.impl;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.europa.ec.simpl.sdtoolingbe.controller.AbstractController;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.error.BaseResponse;
import eu.europa.ec.simpl.sdtoolingbe.error.ErrorResponse;
import eu.europa.ec.simpl.sdtoolingbe.exception.EDCRegistrationException;
import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;
import eu.europa.ec.simpl.sdtoolingbe.logging.LogRequest;
import eu.europa.ec.simpl.sdtoolingbe.service.edcconnector.EDCConnectorService;
import eu.europa.ec.simpl.sdtoolingbe.service.federtedcatalogue.FederatedCatalogueService;
import eu.europa.ec.simpl.sdtoolingbe.service.hashservice.HashService;
import eu.europa.ec.simpl.sdtoolingbe.service.sdservice.SDService;
import eu.europa.ec.simpl.sdtoolingbe.service.validationService.ValidationService;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/api/sd")
@Tag(name = "SD Controller")
@Log4j2
public class SDController extends AbstractController {

    private final EDCConnectorService edcConnectorService;

    private final SDService sdService;

    private final HashService hashService;

    private final ValidationService validationService;

    private final FederatedCatalogueService federatedCatalogueService;

    public SDController(
        EDCConnectorService edcConnectorService,
        SDService sdService,
        HashService hashService,
        ValidationService validationService,
        FederatedCatalogueService federatedCatalogueService
    ) {
        this.edcConnectorService = edcConnectorService;
        this.sdService = sdService;
        this.hashService = hashService;
        this.validationService = validationService;
        this.federatedCatalogueService = federatedCatalogueService;
    }

    @PostMapping(
        path = "/publish", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> publish(
        @Parameter(description = "SD json file", required = true) @RequestParam("json-file") MultipartFile jsonFile,
        HttpServletRequest request
    ) throws Exception {

        try (InputStream inputStream = jsonFile.getInputStream()) {
            String sdJsonLd = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);

            String tier1BearerToken = AuthBearerUtil.getBearerValue(request);

            sdJsonLd = federatedCatalogueService.publishSD(sdJsonLd, tier1BearerToken);

            return new ResponseEntity<>(sdJsonLd, HttpStatus.OK);
        }
    }

    @LogRequest
    @Operation(
        summary = "Register a SD json LD and return it enriched and validated", description = "Add hashings, register asset and policies on EDC connector, create a contract definition on EDC connector and validate the resulting SD json enriched with all info", responses = {
            @ApiResponse(
                responseCode = "200", description = "Successfully enriched and validated SD JSON-LD", content = @Content(
                    mediaType = "application/json", schema = @Schema(
                        implementation = Object.class
                    ), examples = @ExampleObject(
                        name = "SD JSON-LD enriched", value = "Json file enriched and validated"
                    )
                )
            )
        }
    )
    @PostMapping(
        path = "/enrichAndValidate", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<String> enrichAndValidate(
        @Parameter(description = "SD json file", required = true) @RequestParam("json-file") MultipartFile jsonFile,
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "Ecosystem name", example = "simpl"
        ) @RequestParam String ecosystem,
        @Schema(
            requiredMode = Schema.RequiredMode.REQUIRED, description = "File name", example = "data-offeringShape.ttl"
        ) @RequestParam String shapeFileName
    ) throws Exception {

        try (InputStream inputStream = jsonFile.getInputStream()) {
            String sdJsonLd = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);

            sdJsonLd = sdService.setOfferingType(sdJsonLd, ecosystem, shapeFileName);

            sdJsonLd = hashService.generateHashFromJsonLd(sdJsonLd, ecosystem);

            sdJsonLd = edcConnectorService.register(sdJsonLd, ecosystem, shapeFileName);
            sdJsonLd = edcConnectorService.enrich(sdJsonLd, ecosystem);

            validationService.validateJsonLd(
                sdJsonLd,
                shapeFileName,
                ecosystem
            );

            return new ResponseEntity<>(sdJsonLd, HttpStatus.OK);
        }

    }

    @ExceptionHandler(InvalidSDJsonException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ApiResponse(
        responseCode = CODE_BAD_REQUEST, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.BAD_REQUEST)
    public BaseResponse<ErrorResponse> invalidSDJsonException(HttpServletRequest request, InvalidSDJsonException e) {
        log.error("invalidSDJsonException() for request " + request.getRequestURL(), e);
        return createFaultResponse(
            request, HttpStatus.BAD_REQUEST, ErrorCode.INVALID_DOCUMENT, "invalid SD json", e.getMessage()
        );
    }

    @ExceptionHandler(EDCRegistrationException.class)
    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    @ApiResponse(
        responseCode = CODE_SERVICE_UNAVAILABLE, content = @Content(
            mediaType = "application/problem+json", schema = @Schema(implementation = ErrorResponse.class)
        )
    )
    @LogRequest(httpStatus = HttpStatus.SERVICE_UNAVAILABLE)
    public BaseResponse<ErrorResponse> edcRegistrationException(
        HttpServletRequest request, EDCRegistrationException e
    ) {
        log.error("edcRegistrationException() for request " + request.getRequestURL(), e);
        return createFaultResponse(
            request, HttpStatus.SERVICE_UNAVAILABLE, ErrorCode.EDC_CONNECTOR_UNAVAILABLE, "EDC connector error",
            e.getMessage()
        );
    }

}
