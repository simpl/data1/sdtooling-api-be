package eu.europa.ec.simpl.sdtoolingbe;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.TimeZone;

public class TestDateParsing {

	public static void main(String[] args) {
		
		testAny("2024-01-01T10:30:15");
		testAny("2024-01-01T10:30:15Z");
		testAny("2024-01-01T10:30:15+0100");
		testAny("2024-01-01T10:30:15+0200");
		
	}
	
	protected static void testAny(String dateAsString) {
		Instant instant = new DateTimeFormatterBuilder()
			.parseCaseInsensitive()
            .appendPattern("yyyy-MM-dd'T'HH:mm:ss[Z][z]")
            .toFormatter()
            .withZone(ZoneId.of("UTC"))
            .parse(dateAsString, Instant::from)
        ;
		System.out.println("result: " + Date.from(instant));
	}

	protected static void testLocal(String dateAsString) {
		LocalDateTime datetime = LocalDateTime.parse(dateAsString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    	Instant instant = ZonedDateTime.of(datetime, TimeZone.getDefault().toZoneId()).toInstant();
    	System.out.println("local: " + Date.from(instant));
	}
	
	protected static void testOffset(String dateAsString) {
		ZonedDateTime datetime = ZonedDateTime.parse(dateAsString, DateTimeFormatter.ISO_DATE_TIME);
		Instant instant = datetime.toInstant();
    	System.out.println("offset: " + Date.from(instant));
	}

}
