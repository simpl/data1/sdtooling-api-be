FROM maven:3.9.4-eclipse-temurin-21-alpine AS builder

WORKDIR /app

COPY pom.xml .  
COPY src ./src  


RUN mvn clean package -DskipTests


FROM eclipse-temurin:21

RUN groupadd -g 1001 simplgroup && useradd -u 1001 -g simplgroup -m simpluser

WORKDIR /home/simpluser

#copy the pipeline.variables.sh file for return microservice version in /status endpoint
COPY pipeline.variables.sh .

COPY shapes ./tmp/shapes/

COPY entrypoint.sh .
RUN chmod +x /home/simpluser/entrypoint.sh
COPY --from=builder /app/target/*.jar app.jar
RUN mkdir -p /home/simpluser/data
RUN chown -R simpluser:simplgroup /home/simpluser
RUN chmod -R 770 /home/simpluser
#signature verify from gosu uffical docs, fixed gosu version. Gosu used to switch env context from root to simpluser as normal sudo does not work as expected
ENV GOSU_VERSION=1.17
RUN set -eux; \
# save list of currently installed packages for later so we can clean up
	savedAptMark="$(apt-mark showmanual)"; \
	apt-get update; \
	apt-get install -y --no-install-recommends ca-certificates gnupg wget; \
	rm -rf /var/lib/apt/lists/*; \
	\
	dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
	wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
	wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
	\
# verify the signature
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
	gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
	gpgconf --kill all; \
	rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc; \
	\
# clean up fetch dependencies
	apt-mark auto '.*' > /dev/null; \
	[ -z "$savedAptMark" ] || apt-mark manual $savedAptMark; \
	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	\
	chmod +x /usr/local/bin/gosu; \
# verify that the binary works
	gosu --version; \
	gosu nobody true

ENTRYPOINT ["/bin/sh", "-c", "chown -R simpluser:simplgroup /home/simpluser/data && exec gosu simpluser /bin/sh -c '/home/simpluser/entrypoint.sh && exec java -jar /home/simpluser/app.jar'"]



