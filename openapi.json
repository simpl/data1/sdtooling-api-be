{
	"openapi": "3.0.1",
	"info": {
		"title": "SD Tooling Application API",
		"description": "OpenApi documentation for the SD Tooling Application API",
		"version": "1.0"
	},
	"servers": [
		{
			"url": "https://creation-wizard-api.dev.simpl-europe.eu"
		}
	],
	"security": [
		{
			"bearerAuth": []
		}
	],
	"paths": {
		"/api/sd/publish": {
			"post": {
				"tags": [
					"SD Controller"
				],
				"operationId": "publish",
				"requestBody": {
					"content": {
						"multipart/form-data": {
							"schema": {
								"required": [
									"json-file"
								],
								"type": "object",
								"properties": {
									"json-file": {
										"type": "string",
										"description": "SD json file",
										"format": "binary"
									}
								}
							}
						}
					}
				},
				"responses": {
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"503": {
						"description": "Service Unavailable",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"type": "string"
								}
							}
						}
					}
				}
			}
		},
		"/api/sd/enrichAndValidate": {
			"post": {
				"tags": [
					"SD Controller"
				],
				"summary": "Register a SD json LD and return it enriched and validated",
				"description": "Add hashings, register asset and policies on EDC connector, create a contract definition on EDC connector and validate the resulting SD json enriched with all info",
				"operationId": "enrichAndValidate",
				"parameters": [
					{
						"name": "ecosystem",
						"in": "query",
						"description": "Ecosystem name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "Ecosystem name",
							"example": "simpl"
						},
						"example": "simpl"
					},
					{
						"name": "shapeFileName",
						"in": "query",
						"description": "File name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "File name",
							"example": "data-offeringShape.ttl"
						},
						"example": "data-offeringShape.ttl"
					}
				],
				"requestBody": {
					"content": {
						"multipart/form-data": {
							"schema": {
								"required": [
									"json-file"
								],
								"type": "object",
								"properties": {
									"json-file": {
										"type": "string",
										"description": "SD json file",
										"format": "binary"
									}
								}
							}
						}
					}
				},
				"responses": {
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"503": {
						"description": "Service Unavailable",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "Successfully enriched and validated SD JSON-LD",
						"content": {
							"application/json": {
								"schema": {
									"type": "object"
								},
								"examples": {
									"SD JSON-LD enriched": {
										"description": "SD JSON-LD enriched",
										"value": "Json file enriched and validated"
									}
								}
							}
						}
					}
				}
			}
		},
		"/api/policy/usage": {
			"post": {
				"tags": [
					"Policy Controller"
				],
				"summary": "Generate usage policy json-ld",
				"description": "Returns the usage policy json-ld for the specified input parameters passed as json",
				"operationId": "getUsagePolicyJsonLD",
				"requestBody": {
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/schemas/UsagePolicyRequest"
							}
						}
					},
					"required": true
				},
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/OdrlPolicy"
								}
							}
						}
					}
				}
			}
		},
		"/api/policy/access": {
			"post": {
				"tags": [
					"Policy Controller"
				],
				"summary": "Generate access policy json-ld",
				"description": "Returns the access policy json-ld for the specified input parameters passed as json",
				"operationId": "getAccessPolicyJsonLD",
				"requestBody": {
					"content": {
						"application/json": {
							"schema": {
								"$ref": "#/components/schemas/AccessPolicyRequest"
							}
						}
					},
					"required": true
				},
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"$ref": "#/components/schemas/OdrlPolicy"
								}
							}
						}
					}
				}
			}
		},
		"/api/contract/v1/validate": {
			"post": {
				"tags": [
					"Contract Controller"
				],
				"summary": "Return json validated",
				"description": "Return the json-ld validated",
				"operationId": "contractValidate",
				"parameters": [
					{
						"name": "ecosystem",
						"in": "query",
						"description": "Ecosystem name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "Ecosystem name",
							"example": "simpl"
						},
						"example": "simpl"
					},
					{
						"name": "shapeFileName",
						"in": "query",
						"description": "File name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "File name",
							"example": "contract-templateShape.ttl"
						},
						"example": "contract-templateShape.ttl"
					}
				],
				"requestBody": {
					"content": {
						"multipart/form-data": {
							"schema": {
								"required": [
									"json-file"
								],
								"type": "object",
								"properties": {
									"json-file": {
										"type": "string",
										"description": "SD json file",
										"format": "binary"
									}
								}
							}
						}
					}
				},
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "Successfully validated SD JSON-LD",
						"content": {
							"application/json": {
								"schema": {
									"type": "object"
								},
								"examples": {
									"SD JSON-LD validated": {
										"description": "SD JSON-LD validated",
										"value": "Json file validated"
									}
								}
							}
						}
					}
				}
			}
		},
		"/getTTL": {
			"get": {
				"tags": [
					"Conversion Controller"
				],
				"summary": "Get ttl schema file based on ecosystem and file name",
				"description": "Return the file shape ttl if present",
				"operationId": "getTTL",
				"parameters": [
					{
						"name": "ecosystem",
						"in": "query",
						"description": "Ecosystem name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "Ecosystem name",
							"example": "simpl"
						},
						"example": "simpl"
					},
					{
						"name": "fileName",
						"in": "query",
						"description": "File name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "File name",
							"example": "data-offeringShape.ttl"
						},
						"example": "data-offeringShape.ttl"
					}
				],
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"*/*": {
								"schema": {
									"type": "string"
								}
							}
						}
					}
				}
			}
		},
		"/getAvailableShapesTtl": {
			"get": {
				"tags": [
					"Conversion Controller"
				],
				"summary": "Get all available shapes ttl file",
				"description": "Return all available shapes ttl file",
				"operationId": "getAvailableShapesTtl",
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"*/*": {
								"schema": {
									"type": "object",
									"additionalProperties": {
										"type": "object",
										"additionalProperties": {
											"type": "array",
											"items": {
												"type": "string"
											}
										}
									}
								}
							}
						}
					}
				}
			}
		},
		"/getAvailableShapesTtlCategorized": {
			"get": {
				"tags": [
					"Conversion Controller"
				],
				"summary": "Get all available shapes ttl file based on ecosystem",
				"description": "Return all available shapes ttl file based on ecosystem",
				"operationId": "getAvailableShapesTtlCategorized",
				"parameters": [
					{
						"name": "ecosystem",
						"in": "query",
						"description": "Ecosystem name",
						"required": true,
						"schema": {
							"type": "string",
							"description": "Ecosystem name",
							"example": "simpl"
						},
						"example": "simpl"
					}
				],
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"*/*": {
								"schema": {
									"type": "object",
									"additionalProperties": {
										"type": "array",
										"items": {
											"type": "string"
										}
									}
								}
							}
						}
					}
				}
			}
		},
		"/api/policy/identity/attributes": {
			"get": {
				"tags": [
					"Policy Controller"
				],
				"summary": "Get CONSUMER type identity attributes",
				"description": "Returns all the identity attributes assigned to type CONSUMER from the Governance Authority",
				"operationId": "getIdentityAttributes",
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"*/*": {
								"schema": {
									"type": "array",
									"items": {
										"$ref": "#/components/schemas/IdentityAttribute"
									}
								}
							}
						}
					}
				}
			}
		},
		"/api/policy/access/actions": {
			"get": {
				"tags": [
					"Policy Controller"
				],
				"summary": "Get access policy actions",
				"description": "Returns all possible access policy actions",
				"operationId": "getAccessPolicyActions",
				"responses": {
					"500": {
						"description": "Internal Server Error",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"400": {
						"description": "Bad Request",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"401": {
						"description": "Unauthorized",
						"content": {
							"application/problem+json": {
								"schema": {
									"$ref": "#/components/schemas/ErrorResponse"
								}
							}
						}
					},
					"200": {
						"description": "OK",
						"content": {
							"application/json": {
								"schema": {
									"type": "array",
									"items": {
										"$ref": "#/components/schemas/PolicyAction"
									}
								}
							}
						}
					}
				}
			}
		}
	},
	"components": {
		"schemas": {
			"ErrorResponse": {
				"type": "object",
				"properties": {
					"errorTitle": {
						"type": "string"
					},
					"errorDescription": {
						"type": "string"
					}
				}
			},
			"DeletionConstraint": {
				"required": [
					"type"
				],
				"type": "object",
				"allOf": [
					{
						"$ref": "#/components/schemas/UsagePolicyConstraint"
					},
					{
						"type": "object",
						"properties": {
							"type": {
								"type": "string",
								"description": "type of the constraint",
								"example": "Deletion"
							},
							"afterUse": {
								"type": "boolean",
								"description": "after use flag (default is false)"
							}
						}
					}
				]
			},
			"RestrictedDurationConstraint": {
				"required": [
					"type"
				],
				"type": "object",
				"allOf": [
					{
						"$ref": "#/components/schemas/UsagePolicyConstraint"
					},
					{
						"type": "object",
						"properties": {
							"type": {
								"type": "string",
								"description": "type of the constraint",
								"example": "RestrictedDuration"
							},
							"fromDatetime": {
								"type": "string",
								"description": "duration start datetime in ISO8601 format with optional timezone (default is GMT)",
								"example": "2024-08-01T00:00:00Z"
							},
							"toDatetime": {
								"type": "string",
								"description": "duration end datetime in ISO8601 format with optional timezone (default is GMT)",
								"example": "2024-08-31T23:59:59Z"
							}
						}
					}
				]
			},
			"RestrictedNumberConstraint": {
				"required": [
					"maxCount",
					"type"
				],
				"type": "object",
				"allOf": [
					{
						"$ref": "#/components/schemas/UsagePolicyConstraint"
					},
					{
						"type": "object",
						"properties": {
							"type": {
								"type": "string",
								"description": "type of the constraint",
								"example": "RestrictedNumber"
							},
							"maxCount": {
								"type": "integer",
								"description": "restricted number max usage count",
								"format": "int32",
								"example": 10
							}
						}
					}
				]
			},
			"UsagePolicyConstraint": {
				"type": "object",
				"properties": {
					"type": {
						"type": "string"
					}
				},
				"description": "permission contraints",
				"discriminator": {
					"propertyName": "type"
				}
			},
			"UsagePolicyPermission": {
				"required": [
					"assignee",
					"constraints"
				],
				"type": "object",
				"properties": {
					"assignee": {
						"type": "string",
						"description": "permission assignee",
						"example": "Consumer"
					},
					"action": {
						"type": "string",
						"description": "type of permission action (default USE)",
						"enum": [
							"USE"
						]
					},
					"constraints": {
						"type": "array",
						"description": "permission contraints",
						"items": {
							"oneOf": [
								{
									"$ref": "#/components/schemas/DeletionConstraint"
								},
								{
									"$ref": "#/components/schemas/RestrictedDurationConstraint"
								},
								{
									"$ref": "#/components/schemas/RestrictedNumberConstraint"
								}
							]
						}
					}
				},
				"description": "usage permissions"
			},
			"UsagePolicyRequest": {
				"required": [
					"permissions"
				],
				"type": "object",
				"properties": {
					"permissions": {
						"type": "array",
						"description": "usage permissions",
						"items": {
							"$ref": "#/components/schemas/UsagePolicyPermission"
						}
					}
				}
			},
			"OdrlAssignee": {
				"type": "object",
				"properties": {
					"uid": {
						"type": "string"
					},
					"role": {
						"type": "string"
					}
				}
			},
			"OdrlAssigner": {
				"type": "object",
				"properties": {
					"uid": {
						"type": "string"
					},
					"role": {
						"type": "string"
					}
				}
			},
			"OdrlConstraint": {
				"type": "object",
				"properties": {
					"leftOperand": {
						"type": "string"
					},
					"operator": {
						"type": "string"
					},
					"rightOperand": {
						"type": "string"
					}
				}
			},
			"OdrlPermission": {
				"type": "object",
				"properties": {
					"target": {
						"type": "string"
					},
					"assignee": {
						"$ref": "#/components/schemas/OdrlAssignee"
					},
					"action": {
						"type": "array",
						"items": {
							"type": "string"
						}
					},
					"constraint": {
						"type": "array",
						"items": {
							"$ref": "#/components/schemas/OdrlConstraint"
						}
					}
				}
			},
			"OdrlPolicy": {
				"type": "object",
				"properties": {
					"profile": {
						"type": "string"
					},
					"target": {
						"type": "string"
					},
					"assigner": {
						"$ref": "#/components/schemas/OdrlAssigner"
					},
					"uid": {
						"type": "string"
					},
					"@context": {
						"type": "string"
					},
					"@type": {
						"type": "string"
					},
					"permission": {
						"type": "array",
						"items": {
							"$ref": "#/components/schemas/OdrlPermission"
						}
					}
				}
			},
			"AccessPolicyPermission": {
				"required": [
					"action",
					"assignee"
				],
				"type": "object",
				"properties": {
					"assignee": {
						"type": "string",
						"description": "permission assignee"
					},
					"action": {
						"type": "string",
						"description": "type of permission action",
						"enum": [
							"SEARCH",
							"CONSUME",
							"RESTRICTED_CONSUME"
						]
					},
					"fromDatetime": {
						"type": "string",
						"description": "permission start datetime in ISO8601 format with optional timezone (default is GMT)",
						"example": "2024-08-01T00:00:00Z"
					},
					"toDatetime": {
						"type": "string",
						"description": "permission end datetime in ISO8601 format with optional timezone (default is GMT)",
						"example": "2024-08-31T23:59:59Z"
					}
				},
				"description": "access permissions"
			},
			"AccessPolicyRequest": {
				"required": [
					"permissions"
				],
				"type": "object",
				"properties": {
					"permissions": {
						"type": "array",
						"description": "access permissions",
						"items": {
							"$ref": "#/components/schemas/AccessPolicyPermission"
						}
					}
				}
			},
			"IdentityAttribute": {
				"type": "object",
				"properties": {
					"identifier": {
						"type": "string"
					},
					"code": {
						"type": "string"
					}
				}
			},
			"PolicyAction": {
				"type": "object",
				"properties": {
					"label": {
						"type": "string"
					},
					"value": {
						"type": "string"
					}
				}
			}
		},
		"securitySchemes": {
			"bearerAuth": {
				"type": "http",
				"description": "IAA cloud gateway JWT token",
				"name": "bearerAuth",
				"in": "header",
				"scheme": "bearer",
				"bearerFormat": "JWT"
			}
		}
	}
}