package eu.europa.ec.simpl.sdtoolingbe.util.feign;

import eu.europa.ec.simpl.client.core.ssl.SslInfo;
import eu.europa.ec.simpl.client.feign.DaggerFeignSimplClientFactory;
import eu.europa.ec.simpl.client.feign.FeignSimplClient;
import eu.europa.ec.simpl.sdtoolingbe.client.federatedcatalog.FederatedCatalogueTier2Client;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import feign.Feign;
import feign.Logger;

public final class FeignUtil {

    private FeignUtil() {
    }

    /**
     * 
     * @param sslInfo
     * @param bearerToken
     * @param authorityTLSUri
     * @param targetUrl
     * @param loggerLevel     (optional: null) default NONE
     * @return
     */
    public static FederatedCatalogueTier2Client getClient(
        SslInfo sslInfo, String bearerToken, String authorityTLSUri, String targetUrl, Logger.Level loggerLevel
    ) {
        FeignSimplClient simplClient = DaggerFeignSimplClientFactory.create().get();
        Feign.Builder builder = simplClient
            .builder()
            .setSslInfoSupplier(() -> sslInfo)
            // access token al catalogo
            .setAuthorizationHeaderSupplier(() -> AuthBearerUtil.toBearerString(bearerToken))

            // prova effimera in cache (altrimenti la richiede tutte le volte)
            // .setEphemeralProofAdapter(ephemeralProofAdapter)

            .setAuthorityUrlSupplier(() -> authorityTLSUri)
            .build();
        builder.logLevel(loggerLevel == null ? Logger.Level.NONE : loggerLevel);
        return builder.target(FederatedCatalogueTier2Client.class, targetUrl);
    }

}
