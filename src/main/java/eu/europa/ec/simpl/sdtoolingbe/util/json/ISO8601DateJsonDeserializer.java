package eu.europa.ec.simpl.sdtoolingbe.util.json;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;

/**
 * Jackson JSON deserializer che converte una stringa in formato ISO8601 con o senza time zone in un oggetto Date.
 * 
 * @author apadula
 */
public class ISO8601DateJsonDeserializer extends JsonDeserializerSupport<Date> {

    @Override
    public Date deserialize(JsonParser parser, DeserializationContext context)
        throws IOException, JsonProcessingException {
        String dateAsString = parser.getText();
        if (StringUtils.isBlank(dateAsString)) {
            return null;
        }
        try {
            /*
             * accetta i formati con o senza il timezone (senza assume GMT): '2011-12-03T10:15:30'
             * '2011-12-03T10:15:30Z' '2011-12-03T10:15:30+01:00' '2011-12-03T10:15:30+01:00[Europe/Paris]'
             */
            Instant instant = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .appendPattern("yyyy-MM-dd'T'HH:mm:ss[Z][z]")
                .toFormatter()
                .withZone(ZoneId.of("UTC"))
                .parse(dateAsString, Instant::from);
            return Date.from(instant);
        } catch (DateTimeParseException ex) {
            throw newUnparsableDateTimeException(parser);
        }
    }

}
