# SDToolingBe changelog

---
## v1.6.0 (Sprint 11)

#### Dependencies upgrade
- simpl-http-client-feign from 0.7.3 to 1.0.0

---
## v1.5.0 (Sprint 10)

#### Security patches:
- shacl dependency com.google.protobuf/protobuf-java from 3.24.3 to last safe version 4.29.2

#### Values
- **web.mvc.bearer-token.allowed-paths**: (added) 
> optional list (comma separated) of allowed paths (ant matcher expressions) to skip JWT token check (ex: actuator health paths)

---
## v1.4.0 (Sprint 9)

#### Required components
All Aruba components must updated to the following versions. A new authentication-provider must be installed and configured.

- name: simpl-cloud-gateway
  repository: https://code.europa.eu/api/v4/projects/772/packages/helm/stable
  version: 0.8.0
- name: users-roles
  version: 0.8.1
  repository: https://code.europa.eu/api/v4/projects/771/packages/helm/stable
- name: simpl-fe
  version: 0.8.0
  repository: https://code.europa.eu/api/v4/projects/769/packages/helm/stable
- name: tls-gateway
  repository: https://code.europa.eu/api/v4/projects/860/packages/helm/stable
  version: 0.8.1
- name: authentication-provider
  version: 0.8.1
  repository: https://code.europa.eu/api/v4/projects/939/packages/helm/stable

#### Values
- **usersRoles.apiUrl**: (changed) comment changed

from

```
# url to the Users&Roles microservice API until the user-api context path (excluding the controllers path)
```
to

```
# url to the Users&Roles service through the tier1 gateway
```

- **edcConnector.tier2BaseUrl**: (added) is the tier2 url to the provider EDC connector service behind the TLS gateway
> add this new property to the edcConnector keeping the others as is 

```
edcConnector:
    ...
  # tier2 url to the provider EDC connector service behind the TLS gateway (ending with the route context name)
  # this will be enriched in the SD json by enrichAndValidate API
    tier2BaseUrl: https://tls.participant.provider.dev.simpl-europe.eu/edc
```

- **authenticationProvider.apiUrl**: (added) is the Aruba authentication-provider internal service http url and port
> set the apiUrl to your internal authentication-provider service domain and port 

```
# url to the AuthenticationProvider internal service
authenticationProvider:
  apiUrl: http://authentication-provider:8080
```

- **federatedCatalogue.tier2Gateway.pathPrefix**: (changed) mtls can be removed because it's not longer mandatory
example: pathPrefix can start just with the service name

```
# federated catalog application (fc-service)
federatedCatalogue:
 ...
    # tier2 tls gateway path configured as route rule to the federated catalog application
    pathPrefix: /fc-service
```

#### Tier1 gateway configuration
- The 'private' prefix now can be removed from the predicates path setting StripPrefix to 1.

example:
```
        - id: sdtooling-wizard-be
              uri: ...
              predicates:
                - Path=/sdtooling-api/**
              filters:
                - StripPrefix=1
```

- RBAC rules changed adding the SD_PUBLISHER role.

example:

```
        - path: "/sdtooling-api/**"
          method: GET
          roles:
            - SD_PUBLISHER
        - path: "/sdtooling-api/**"
          method: POST
          roles:
            - SD_PUBLISHER
        - path: "/user-api/agent/identity-attributes"
          method: GET
          roles:
            - T1UAR_M
            - SD_PUBLISHER
        - path: "/user-api/agent/identity-attributes/*"
          method: GET
          roles:
            - T1UAR_M
            - SD_PUBLISHER
        - path: "/user-api/identity-attribute/search"
          method: GET
          roles:
            - T1UAR_M
            - SD_PUBLISHER
```

#### Tier2 gateway configuration
- The catalogue service (fc-service) must be configured on the Authority Agent.

> set the uri to your internal fc-service service domain and port  

example:

```
        - id: fc-service
              uri: http://fc-service.gaiax-edc-dev-catalogue.svc.cluster.local:8081
              predicates:
                - Path=/fc-service/self-descriptions/**
              filters:
                - StripPrefix=1
```

- The catalogue adapter service (catalogue-adapter-service) must be configured on the Authority Agent.
> set the uri to your internal catalogue-adapter-service service domain and port  

example:

```
        - id: catalogue-adapter-service
              uri: http://catalogue-adapter-service.gaiax-edc-dev-catalogue.svc.cluster.local:8084
              predicates:
                - Path=/catalogue-adapter-service/**
              filters:
                - StripPrefix=1
```

- ABAC rules changed adding the DATA_PROVIDER_PUBLISHER dentity attributes for the following catalogue service endpoints.

example:

```
        - path: /fc-service/self-descriptions
          method: POST
          identity-attributes:
            - DATA_PROVIDER_PUBLISHER
        - path: /fc-service/self-descriptions/**
          method: GET
          identity-attributes:
           - DATA_PROVIDER_PUBLISHER
```

#### Keycloak Users & Roles
In the data provider participant keycloak must be created a new user named 'publisher.sd' and a new role 'SD_PUBLISHER'.

Users/Roles binding:
|User|Roles|
|--------|----|
|publisher.sd|SD_PUBLISHER|

#### Auhority Identity Attributes
In the Authority keycloak the follwing identity attributes must be configured and binded.

Identity attributes/Roles binding:
|Identity Attribute|Roles|
|--------|------|
|DATA_PROVIDER_PUBLISHER|SD_PUBLISHER|

