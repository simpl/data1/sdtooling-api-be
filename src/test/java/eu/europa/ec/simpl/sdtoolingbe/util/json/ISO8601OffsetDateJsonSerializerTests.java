package eu.europa.ec.simpl.sdtoolingbe.util.json;

import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;

class ISO8601OffsetDateJsonSerializerTests {
	
	@InjectMocks
    private ISO8601OffsetDateJsonSerializer serializer;

    @Mock
    private JsonGenerator jsonGenerator;

    @Mock
    private SerializerProvider serializerProvider;
    
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }
	
	@Test
    void testSerializeWithValidDate() throws IOException {
		Instant instant = Instant.parse("2023-02-28T14:39:00.137Z"); // GMT time
        Date date = Date.from(instant);

        // Execute
        serializer.serialize(date, jsonGenerator, serializerProvider);

        // Assert
        ZonedDateTime expectedDateTime = ZonedDateTime.of(
                LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()), 
                ZoneId.systemDefault()
        );
        String expectedDateString = expectedDateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        verify(jsonGenerator).writeString(expectedDateString);
    }

	@Test
    void testSerializeWithNullDate() throws IOException {
        // Execute
        serializer.serialize(null, jsonGenerator, serializerProvider);

        // Assert
        verify(jsonGenerator).writeNull();
    }
	
}
