package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ContractDefinition extends AbstractManagement {
    
    private String accessPolicyId;

    private String contractPolicyId;

    @JsonProperty("assetsSelector")
    private List<AssetsSelector> assetsSelectors = new ArrayList<>();

    public ContractDefinition(String assetId, String accessPolicyId, String contractPolicyId) {
        this.accessPolicyId = accessPolicyId;
        this.contractPolicyId = contractPolicyId;
        this.assetsSelectors.add(new AssetsSelector(assetId));
    }

}
