package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OdrlConstraint {

    private String leftOperand;
    private String operator;
    private String rightOperand;

    public OdrlConstraint(String leftOperand, String operator, String rightOperand) {
        this.leftOperand = leftOperand;
        this.operator = operator;
        this.rightOperand = rightOperand;
    }

}
