package eu.europa.ec.simpl.sdtoolingbe.model.edc.management;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
public abstract class AbstractManagement {

    @JsonProperty("@context")
    private final Context context;

    private Map<String, String> properties;

    /*
     * @context": { "@vocab": "https://w3id.org/edc/v0.0.1/ns/" }
     */
    protected AbstractManagement() {
        this.context = new Context("https://w3id.org/edc/v0.0.1/ns/");
    }

    public void setProperty(String key, String value) {
        if (properties == null) {
            properties = new HashMap<>();
        }
        properties.put(key, value);
    }
}
