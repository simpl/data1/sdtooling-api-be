package eu.europa.ec.simpl.sdtoolingbe.properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import eu.europa.ec.simpl.sdtoolingbe.dto.HashModel;


class HashPropertiesTests {

	private static final String TEMPLATE_LIST =
			"""
					[
						{
							"key": "Contract Template 1",
								"value": "https://files.dev.simpleurope.eu/static/pdf/ContractTemplate1.pdf"
						},
						{
							"key": "Billing Schema 1",
								"value": "https://files.dev.simpleurope.eu/static/pdf/BillingSchema1.pdf"
						}
					]
					""";


	private static final String MODEL_LIST =
			"""
						[
						                {
						                  "hashObj": "contractTemplate",
						                  "hashDoc": "contractTemplateDocument",
						                  "hashAlg": "contractTemplateHashAlg",
						                  "hashValue": "contractTemplateHashValue",
						                  "hashUrl": "contractTemplateURL"
						                },
						                {
						                  "hashObj": "billingSchema",
						                  "hashDoc": "billingSchemaDocument",
						                  "hashAlg": "billingSchemaHashAlg",
						                  "hashValue": "billingSchemaHashValue",
						                  "hashUrl": "billingSchemaURL"
						                }
						]
					""";

	@Test
	void testModel() {
		HashProperties properties = new HashProperties();
		properties.setHashAlgorithm("algorithm");
		properties.setHashModelList(MODEL_LIST);
		properties.setHashTemplateList(TEMPLATE_LIST);


		assertEquals("algorithm", properties.getHashAlgorithm());

		List<Template> expectedTemplateList = Template.loadTemplates(TEMPLATE_LIST);
		List<Template> actualTemplateList = properties.getHashTemplateList();

		for (int i = 0; i < expectedTemplateList.size(); i++) {
			Template expectedTemplate = expectedTemplateList.get(i);
			Template actualTemplate = actualTemplateList.get(i);

			assertEquals(expectedTemplate.getKey(), actualTemplate.getKey());
			assertEquals(expectedTemplate.getValue(), actualTemplate.getValue());
		}

		List<HashModel> expectedModelList = HashModel.loadModels(MODEL_LIST);
		List<HashModel> actualModelList = properties.getHashModelList();

		for (int i = 0; i < expectedModelList.size(); i++) {
			HashModel expectedModel = expectedModelList.get(i);
			HashModel actualModel = actualModelList.get(i);

			assertEquals(expectedModel.getHashDoc(), actualModel.getHashDoc());
			assertEquals(expectedModel.getHashUrl(), actualModel.getHashUrl());
			assertEquals(expectedModel.getHashValue(), actualModel.getHashValue());
			assertEquals(expectedModel.getHashAlg(), actualModel.getHashAlg());
			assertEquals(expectedModel.getHashObj(), actualModel.getHashObj());
		}

	}

}