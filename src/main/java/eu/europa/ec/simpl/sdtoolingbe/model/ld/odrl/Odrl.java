package eu.europa.ec.simpl.sdtoolingbe.model.ld.odrl;

import eu.europa.ec.simpl.sdtoolingbe.model.ld.AbstractJsonLD;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class Odrl extends AbstractJsonLD {

    public static final String ODRL_NS_PREFIX_V2 = "http://www.w3.org/ns/odrl/2/";

    /*
     * "@context": "http://www.w3.org/ns/odrl.jsonld", "@type": "{type}", "uid": "{uid}", "profile":
     * "http://www.w3.org/ns/odrl/2/odrl.jsonld",
     */
    protected Odrl(String uid, String type) {
        super(uid, "http://www.w3.org/ns/odrl.jsonld");
        setType(type);
        setProfile(ODRL_NS_PREFIX_V2 + "odrl.jsonld");
    }

}
