package eu.europa.ec.simpl.sdtoolingbe.service.usersroles;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import eu.europa.ec.simpl.sdtoolingbe.client.usersroles.UsersRolesClient;
import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;
import eu.europa.ec.simpl.sdtoolingbe.model.client.identityattribute.IdentityAttribute;
import eu.europa.ec.simpl.sdtoolingbe.util.web.AuthBearerUtil;
import feign.FeignException;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UsersRolesServiceImpl implements UsersRolesService {

    private UsersRolesClient usersRolesClient;

    public UsersRolesServiceImpl(UsersRolesClient usersRolesClient) {
        this.usersRolesClient = usersRolesClient;
    }

    @Override
    public List<IdentityAttribute> getIdentityAttributes(String bearerToken, String participantType)
        throws UnauthorizedException {
        List<IdentityAttribute> result = new ArrayList<>();
        try {
            String json = usersRolesClient.getAgentIdentityAttributes(AuthBearerUtil.toBearerString(bearerToken));
            JSONArray jsonArray = new JSONArray(json);
            jsonArray.forEach((Object item) -> {
                JSONObject obj = (JSONObject) item;
                JSONArray participantTypes = obj.optJSONArray("participantTypes");
                if (participantTypes != null && participantTypes.toList().contains(participantType)) {
                    IdentityAttribute identityAttribute = new IdentityAttribute(
                        obj.getString("name"), obj.getString("code")
                    );
                    result.add(identityAttribute);
                }
            });

            return result;
        } catch (FeignException.Unauthorized e) {
            log.error("getIdentityAttributes() failed cause {}", e);
            throw new UnauthorizedException("invalid bearer token");
        }
    }

}
