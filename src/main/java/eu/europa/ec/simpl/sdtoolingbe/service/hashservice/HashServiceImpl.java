package eu.europa.ec.simpl.sdtoolingbe.service.hashservice;

import java.util.List;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import eu.europa.ec.simpl.sdtoolingbe.dto.HashModel;
import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.exception.BadRequestException;
import eu.europa.ec.simpl.sdtoolingbe.properties.HashProperties;
import eu.europa.ec.simpl.sdtoolingbe.util.HashGeneratorUtil;
import lombok.extern.log4j.Log4j2;

/**
 * Hash generation service
 */
@Service
@Log4j2
public class HashServiceImpl implements HashService {

    private static final String LOG_INIT = "Init";

    private final String hashAlg;
    private final List<HashModel> hashModelList;

    private final HashProperties hashProperties;

    private final HashGeneratorUtil hashGeneratorUtil;

    public HashServiceImpl(HashProperties hashProperties, HashGeneratorUtil hashGeneratorUtil) {
        this.hashProperties = hashProperties;

        this.hashAlg = hashProperties.getHashAlgorithm();
        this.hashModelList = hashProperties.getHashModelList();
        this.hashGeneratorUtil = hashGeneratorUtil;
    }

    @Override
    public String generateHashFromJsonLd(String jsonLd, String ecosystem) throws Exception {
        String method = "Method: generateHashFromJsonLd - ";
        log.info("{}{}", method, LOG_INIT);

        JsonNode jsonFile;
        try {
            jsonFile = new ObjectMapper().readTree(jsonLd);
        } catch (Exception e) {
            log.error("{}Convert jsonLd to JsonNode exception: {}", method, e.getMessage());
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "Input Json-Ld not valid");
        }
        if (jsonFile == null) {
            log.info("{}Input Json-Ld not valid: {}", method, jsonLd);
            throw new BadRequestException(ErrorCode.GENERAL_ERROR.getKeyErrorMessage(), "Input Json-Ld not valid");
        }

        for (HashModel hashModel : hashModelList) {
            generateHashJsonLd(ecosystem, jsonFile, hashModel);
        }

        return jsonFile.toString();
    }

    private void generateHashJsonLd(String ecosystem, JsonNode jsonFile, HashModel hashModel) throws Exception {
        String method = "Method: generateHashJson - ";
        log.info("{}{}", method, LOG_INIT);

        if (jsonFile.has(ecosystem + ":" + hashModel.getHashObj())) {
            JsonNode hashObj = jsonFile.get(ecosystem + ":" + hashModel.getHashObj());
            log.info("{}Object found: {}", method, hashObj);

            String hashDoc = hashObj.get(ecosystem + ":" + hashModel.getHashDoc()).textValue();
            log.info("{}Hash document value: {}", method, hashModel.getHashDoc());

            String hashValue = hashGeneratorUtil.generateHashValue(hashDoc);
            String hashUrl = hashProperties.getValueFromKey(hashDoc);

            ObjectNode hashObjNew = (ObjectNode) hashObj;

            hashObjNew.put(ecosystem + ":" + hashModel.getHashAlg(), hashAlg);
            log.info("{}Put hash alg: {}", method, hashAlg);

            hashObjNew.put(ecosystem + ":" + hashModel.getHashValue(), hashValue);
            log.info("{}Put hash value: {}", method, hashValue);

            hashObjNew.put(ecosystem + ":" + hashModel.getHashUrl(), hashUrl);
            log.info("{}Put hash url: {}", method, hashUrl);
        }
    }

}
