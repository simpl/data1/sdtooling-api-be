package eu.europa.ec.simpl.sdtoolingbe.util.security;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.List;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import eu.europa.ec.simpl.sdtoolingbe.TestSupport;

class SecurityUtilTests {
	
	private static final String VALID_KEYPAIR_JSON = 
		"""
			{
			    "publicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEA3UlQoidirebLSc8nlOc67t2lwtI1F4AgY+4UhCzVDGy0Xt7K9C+OIbhSW4MGf9vw/8WVYIMaJS61ysGnYPSRQ==",
			    "privateKey": "MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgq3DCWu8A+qRrPYDDV8LTUFqG+BF7nu4Y1EBrNXDwOfGgCgYIKoZIzj0DAQehRANCAAQDdSVCiJ2Kt5stJzyeU5zru3aXC0jUXgCBj7hSELNUMbLRe3sr0L44huFJbgwZ/2/D/xZVggxolLrXKwadg9JF"
			}
		"""
	;

    @Test
    void testCreatePrivateKey() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        byte[] encodedKey = keyPair.getPrivate().getEncoded();

        PrivateKey privateKey = SecurityUtil.createPrivateKey(encodedKey, "EC");

        assertNotNull(privateKey);
    }

    @Test
    void testCreateX509Certificates() throws Exception {
        String certificatePem = TestSupport.getResourceAsString("test/data-provider-eng-pcert", null);

        List<Certificate> certificates = SecurityUtil.createX509Certificates(certificatePem);

        assertNotNull(certificates);
        assertFalse(certificates.isEmpty());
        assertTrue(certificates.get(0) instanceof X509Certificate);
    }
    
    @Test
    void testCreateX509CertificatesWithPemPrivateKeyOnly() throws Exception {
    	JSONObject keypairObj = new JSONObject(VALID_KEYPAIR_JSON);
		PrivateKey privateKey = SecurityUtil.createPrivateKey(Base64.getDecoder().decode(keypairObj.getString("privateKey")), "EC");

        String certificatePem = convertToPem(privateKey);

        List<Certificate> certificates = SecurityUtil.createX509Certificates(certificatePem);

        assertNotNull(certificates);
        assertTrue(certificates.isEmpty());
    }

    @Test
    void testCreatePKCS12Keystore() throws Exception {
    	String certificatePem = TestSupport.getResourceAsString("test/data-provider-eng-pcert", null);
    	
    	JSONObject keypairObj = new JSONObject(VALID_KEYPAIR_JSON);
		PrivateKey privateKey = SecurityUtil.createPrivateKey(Base64.getDecoder().decode(keypairObj.getString("privateKey")), "EC");

        List<Certificate> certificates = SecurityUtil.createX509Certificates(certificatePem);
       
        KeyStore keyStore = SecurityUtil.createPKCS12Keystore(privateKey, certificates, "password");

        assertNotNull(keyStore);
        assertTrue(keyStore.containsAlias("alias"));
    }

    @Test
    void testVerifyPrivateKeyMatchesCertificateChain() throws Exception {
    	String certificatePem = TestSupport.getResourceAsString("test/data-provider-eng-pcert", null);
    	
    	JSONObject keypairObj = new JSONObject(VALID_KEYPAIR_JSON);
		PrivateKey privateKey = SecurityUtil.createPrivateKey(Base64.getDecoder().decode(keypairObj.getString("privateKey")), "EC");
    	
        List<Certificate> certificates = SecurityUtil.createX509Certificates(certificatePem);
        X509Certificate certificate = (X509Certificate) certificates.get(0);

        assertDoesNotThrow(() -> SecurityUtil.verifyPrivateKeyMatchesCertificateChain(privateKey, certificate));
    }

    @Test
    void testVerifyPrivateKeyMatchesCertificateChainWithMismatchedKeys() throws Exception {
    	String certificatePem = TestSupport.getResourceAsString("test/data-provider-eng-pcert", null);
    	
    	KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        
        List<Certificate> certificates = SecurityUtil.createX509Certificates(certificatePem);
        X509Certificate certificate = (X509Certificate) certificates.get(0);

        assertThrows(GeneralSecurityException.class, () -> SecurityUtil.verifyPrivateKeyMatchesCertificateChain(keyPair.getPrivate(), certificate));
    }
    
    
    
 

    private String convertToPem(PrivateKey privateKey) {
        StringWriter writer = new StringWriter();
        writer.write("-----BEGIN PRIVATE KEY-----\n");
        String encodedKey = Base64.getEncoder().encodeToString(privateKey.getEncoded());
        writer.write(wrapText(encodedKey, 64)); // Inserisce interruzioni di riga ogni 64 caratteri
        writer.write("-----END PRIVATE KEY-----\n");
        return writer.toString();
    }

    private String wrapText(String text, int lineLength) {
        StringBuilder wrapped = new StringBuilder();
        for (int i = 0; i < text.length(); i += lineLength) {
            wrapped.append(text, i, Math.min(i + lineLength, text.length())).append("\n");
        }
        return wrapped.toString();
    }

}
