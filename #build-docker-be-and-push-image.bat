@ECHO off

set servicename=sdtooling-api-be

ECHO ============================
ECHO SERVICE NAME : %servicename%
ECHO ============================

ECHO ============================
ECHO BUILD BE
ECHO ============================

call mvn clean package -DskipTests

ECHO ============================
ECHO DOCKER LOGIN
ECHO ============================

call docker login code.europa.eu:4567

ECHO ============================
ECHO DOCKER BUILD
ECHO ============================

call docker build -t code.europa.eu:4567/simpl/simpl-open/development/data1/%servicename% .

ECHO ============================
ECHO DOCKER PUSH
ECHO ============================

call docker push code.europa.eu:4567/simpl/simpl-open/development/data1/%servicename%

ECHO ============================
ECHO ALL COMMAND COMPLETED
ECHO ============================

EXIT /B