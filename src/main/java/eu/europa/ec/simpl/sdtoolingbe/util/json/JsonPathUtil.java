package eu.europa.ec.simpl.sdtoolingbe.util.json;

import org.apache.commons.lang3.StringUtils;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

import eu.europa.ec.simpl.sdtoolingbe.exception.InvalidSDJsonException;

public final class JsonPathUtil {

    private JsonPathUtil() {}

    public static String getStringValue(String json, String path, boolean mandatory) throws InvalidSDJsonException {
        try {
            String result = JsonPath.read(json, path);
            if (mandatory && StringUtils.isBlank(result)) {
                throw new InvalidSDJsonException("no value found for path '" + path + "'");
            }
            return result;
        } catch (PathNotFoundException e) {
            throw new InvalidSDJsonException("no element found for path '" + path + "'", e);
        }
    }

}
