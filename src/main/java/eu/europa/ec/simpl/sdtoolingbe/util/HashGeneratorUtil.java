package eu.europa.ec.simpl.sdtoolingbe.util;

import eu.europa.ec.simpl.sdtoolingbe.enumeration.ErrorCode;
import eu.europa.ec.simpl.sdtoolingbe.exception.CustomException;
import eu.europa.ec.simpl.sdtoolingbe.properties.HashProperties;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
@Log4j2
public class HashGeneratorUtil {
    private static final int BUFFER_SIZE = 8192;
    private final HashProperties hashProperties;
    private final String hashAlg;

    public HashGeneratorUtil(HashProperties hashProperties) {
        this.hashProperties = hashProperties;
        this.hashAlg = hashProperties.getHashAlgorithm();
    }

    public String generateHashValue(String document) throws IOException {
        String method = "Method: generateHashValue - ";

        if (document == null) {
            logErrorAndThrow(method, "Document key not found", ErrorCode.DOCUMENT_KEY_NOT_FOUND);
        }

        String link = hashProperties.getValueFromKey(document);
        if (link == null) {
            logErrorAndThrow(method, "URL not found", ErrorCode.URL_NOT_FOUND);
        }

        URL url = validateUrl(link, method);

        validateUrlResponse(url, method);

        return computeHash(url, method);
    }

    private URL validateUrl(String link, String method) {
        try {
            return new URI(link).toURL();
        } catch (IllegalArgumentException | MalformedURLException | URISyntaxException e) {
            log.error("{}{}", method, e.getMessage());
            throw new CustomException(ErrorCode.URL_VALUE_NOT_VALID.getKeyErrorMessage(), "URL value not valid");
        }
    }

    private void validateUrlResponse(URL url, String method) throws IOException {
        HttpURLConnection huc = (HttpURLConnection) url.openConnection();
        huc.setRequestMethod("HEAD");
        int responseCode = huc.getResponseCode();

        if (responseCode != HttpStatus.OK.value()) {
            log.error("{}Received response code {} invoking URL {}", method, responseCode, url);
            throw new CustomException(ErrorCode.URL_VALUE_NOT_VALID.getKeyErrorMessage(), "URL value not valid");
        }
    }

    private String computeHash(URL url, String method) throws IOException {
        MessageDigest digest = getMessageDigest(method);

        try (InputStream inputStream = url.openStream()) {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                assert digest != null;
                digest.update(buffer, 0, bytesRead);
            }
        }

        assert digest != null;
        return convertToHex(digest.digest(), method);
    }

    private MessageDigest getMessageDigest(String method) {
        try {
            return MessageDigest.getInstance(hashAlg);
        } catch (NoSuchAlgorithmException e) {
            log.error("{}{}", method, e.getMessage());
            logErrorAndThrow(method, "Unexpected error", ErrorCode.GENERAL_ERROR);
        }
        return null;
    }

    private String convertToHex(byte[] hashBytes, String method) {
        StringBuilder sb = new StringBuilder();
        for (byte b : hashBytes) {
            sb.append(String.format("%02x", b));
        }
        String hashString = sb.toString();
        log.debug("{}Hash value is: {}", method, hashString);
        return hashString;
    }

    private void logErrorAndThrow(String method, String message, ErrorCode errorCode) {
        log.error("{}{}", method, message);
        throw new CustomException(errorCode.getKeyErrorMessage(), message);
    }

}
