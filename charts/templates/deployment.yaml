apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Release.Name }}-creation-wizard-api
  labels:
    app: sd-api
spec:
  replicas: 1
  selector:
    matchLabels:
      app: sd-api
  template:
    metadata:
      annotations:
#        vault.hashicorp.com/agent-inject: "true"
#        vault.hashicorp.com/role: "gaiax-edc_role"
#        vault.hashicorp.com/agent-inject-secret-config.txt: "{{ .Values.vaultEnvIdentifier }}/{{ .Release.Namespace }}-sd-creation-wizard-api"
#        vault.hashicorp.com/agent-inject-template-config.txt: |
#          {{`{{ with secret "`}}{{ .Values.vaultEnvIdentifier }}/{{ .Release.Namespace }}{{`-sd-creation-wizard-api" }}
#          {{ range $k, $v := .Data.data }}
#            export {{ $k }}={{ $v }}
#          {{ end }}
#          {{ end }}`}}
      labels:
        app: sd-api
    spec:
      volumes:
        - name: data-storage
          persistentVolumeClaim:
            claimName: {{ .Values.schemaVolumeClaim }}
      serviceAccountName: {{ if .Values.serviceAccount }}{{ .Values.serviceAccount.name | default .Release.Name }}{{ else }}{{ .Release.Name }}{{ end }}
      containers:
        - name: {{ .Chart.Name }}-api
          image: {{ .Values.image.repository }}:{{ .Values.image.tag }}
          imagePullPolicy: {{ .Values.pullPolicy }}
          resources: {{ toYaml .Values.resources | nindent 12 }}
          ports:
            - containerPort: {{ .Values.creationWizardApiContainerPort }}
          volumeMounts:
            - name: data-storage
              mountPath: {{ .Values.schemaPvcMountPath }}  # Percorso nel container dove montare il PVC
          env:
            - name: EDC_CONNECTOR_BASE_URL
              value: {{ .Values.edcConnector.baseUrl | quote }}
            - name: EDC_CONNECTOR_TIER2_BASE_URL
              value: {{ .Values.edcConnector.tier2BaseUrl | quote }}  
            - name: EDC_CONNECTOR_PARTICIPANT_ID
              value: {{ .Values.edcConnector.participantId | quote }}

            - name: FEDERATED_CATALOGUE_TIER2_GATEWAY_URI
              value: {{ .Values.federatedCatalogue.tier2Gateway.uri | quote }}
            - name: FEDERATED_CATALOGUE_TIER2_GATEWAY_PATH_PREFIX
              value: {{ .Values.federatedCatalogue.tier2Gateway.pathPrefix | quote }}
            
            - name: HASH_CONFIG_ALGORITHM
              value: {{ .Values.hash.config.algorithm | quote }}
            - name: HASH_CONFIG_TEMPLATES
              value: {{ .Values.hash.config.templates | quote }}
            - name: HASH_CONFIG_MODELS
              value: {{ .Values.hash.config.models | quote }}

            - name: OFFERING_TYPE.CONFIG.TEMPLATES
              value: {{ .Values.offeringType.config.templates | quote }}

            - name: PARTICIPANT_ONBOARDING_CREDENTIAL_PASSWORD
              value: {{ .Values.participant.onboarding.credentialPassword | quote }}
            - name: PARTICIPANT_ONBOARDING_AUTHORITY_TLS_URI
              value: {{ .Values.participant.onboarding.authorityTLSUri | quote }}
            
            - name: USERS_ROLES_API_URL
              value: {{ .Values.usersRoles.apiUrl | quote }}
            - name: AUTHENTICATION_PROVIDER_API_URL
              value: {{ .Values.authenticationProvider.apiUrl | quote }}
              
            - name: VALIDATION_CONFIG_ENABLED
              value: {{ .Values.validation.config.enabled | quote }}
            - name: VALIDATION_CONFIG_DOMAIN
              value: {{ .Values.validation.config.domain | quote }}
            - name: VALIDATION_CONFIG_VALIDATION_API
              value: {{ .Values.validation.config.validationApi | quote }}
            - name: VALIDATION_CONFIG_VALIDATION_JSONLD_API
              value: {{ .Values.validation.config.validationJsonldApi | quote }}

            - name: WEB_MVC_CORS_ALLOWED_ORIGINS
              value: {{ .Values.web.mvc.cors.allowedOrigins | quote }}
            - name: WEB_MVC_BEARER_TOKEN_REQUIRED
              value: {{ .Values.web.mvc.bearerToken.required | quote }}
            - name: WEB_MVC_BEARER_TOKEN_ALLOWED_PATHS
              value: {{ .Values.web.mvc.bearerToken.allowedPaths | quote }}
            
            - name: OPENAPI_CONFIG_SERVERS
              value: {{ .Values.openapiConfig.servers | quote }}

          livenessProbe:
            httpGet:
              path: /actuator/health/liveness
              port: {{ .Values.creationWizardApiContainerPort }}
              httpHeaders:
                - name: Accept
                  value: application/json
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 2
            failureThreshold: 3

          readinessProbe:
            httpGet:
              path: /actuator/health/readiness
              port: {{ .Values.creationWizardApiContainerPort }}
              httpHeaders:
                - name: Accept
                  value: application/json
            initialDelaySeconds: 30
            periodSeconds: 10
            timeoutSeconds: 2
            failureThreshold: 3