package eu.europa.ec.simpl.sdtoolingbe.model.client.identityattribute;

import lombok.Data;

@Data
public class IdentityAttribute {

    private final String identifier;
    private final String code;

}
