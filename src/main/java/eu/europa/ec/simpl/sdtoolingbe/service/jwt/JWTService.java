package eu.europa.ec.simpl.sdtoolingbe.service.jwt;

import eu.europa.ec.simpl.sdtoolingbe.exception.UnauthorizedException;

public interface JWTService {

    void checkJWT(String token) throws UnauthorizedException;

}
